SELECT * FROM biodata bio
JOIN employee em ON em.biodata_id = bio.id
JOIN employee_position emp ON emp.employee_id = em.id
JOIN position pos ON pos.id = emp.position_id

SELECT distinct em.nip, CONCAT(first_name,last_name), SUM(DATEDIFF(Day, lr.start_date, lr.end_date)) [Cuti Diambil]
FROM biodata bio
right JOIN employee em ON em.biodata_id = bio.id
left JOIN leave_request lr ON lr.employee_id = em.id
GROUP BY em.nip, first_name,last_name, lr.leave_id, lr.start_date, lr.end_date

SELECT CONCAT(bio.first_name,' ', bio.last_name) [Fullname],  pos.name, DATEDIFF (year, bio.dob, GETDATE()) [Umur],
COUNT(CASE WHEN fa.status='anak' THEN 1 
	   END) As jml_anak
FROM biodata bio
JOIN employee em ON em.biodata_id = bio.id
JOIN employee_position emp ON emp.employee_id = em.id
JOIN position pos ON pos.id = emp.position_id
left JOIN family fa ON fa.biodata_id = bio.id
GROUP BY bio.first_name, bio.last_name, pos.name, bio.dob, fa.status

SELECT CONCAT(bio.first_name, '', bio.last_name)[Full Name], emp.nip, emp.status, emp.salary
FROM biodata bio
JOIN employee emp ON emp.biodata_id = bio.id
WHERE bio.pob = 'jakarta' AND bio.address NOT LIKE '%jakarta'

SELECT CONCAT(bio.first_name, ' ', bio.last_name)[Full Name], ty.name, tr.start_date, tr.end_date, SUM(DATEDIFF(day, tr.start_date, tr.end_date)*ty.travel_fee)
FROM biodata bio
JOIN employee emp ON emp.biodata_id = bio.id
JOIN travel_request tr ON tr.employee_id = emp.id
JOIN travel_type ty ON ty.id = tr.travel_type_id
GROUP BY bio.first_name, bio.last_name , ty.name, tr.start_date, tr.end_date

SELECT count(CASE WHEN status='kontrak' THEN 1 END)[Kontrak], count(CASE WHEN status='permanen' THEN 1 END) [Permanen]
from employee;

SELECT distinct CONCAT(bio.first_name, ' ', bio.last_name)[Full Name], dept.name
FROM biodata bio
JOIN employee emp ON emp.biodata_id = bio.id

ALTER TABLE employee ADD department_id bigint
UPDATE employee SET department_id = 2 WHERE biodata_id =1
UPDATE employee SET department_id = 3 WHERE biodata_id =2


SELECT CONCAT(bio.first_name,' ', bio.last_name) [Fullname], 
CASE WHEN fa.status='anak' OR fa. status  = 'istri' OR fa. status = 'suami' THEN 'Sudah Menikah' END AS [Status Pernikahan]
FROM biodata bio
JOIN family fa ON fa.biodata_id = bio.id
where fa.status = 'anak'
GROUP BY bio.first_name, bio.last_name,fa. status

SELECT * 
FROM biodata bio
left join employee emp ON bio.id = emp.biodata_id
where emp.id is null

SELECT count(emp.id) [Karyawan]
FROM biodata bio
JOIN employee emp on bio.id = emp.biodata_id
where year(bio.dob) >= '1991' AND  year(bio.dob) <= '1992'


