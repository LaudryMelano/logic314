CREATE DATABASE db_penerbit

CREATE TABLE tblPengarang ( 
 id int primary key identity (1,1),
 kd_Pengarang varchar(7) not null,
 nama varchar(30) not null,
 alamat varchar(80) not null,
 kota varchar(15) not null,
 kelamin varchar(1) not null
)

CREATE TABLE tblGaji (
 id int primary key identity (1,1),
 kd_pengarang varchar(7) not null,
 nama varchar(30) not null,
 gaji decimal(18,4) not null 
)

INSERT INTO tblPengarang(kd_Pengarang,nama,alamat,kota,kelamin)
values
('P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
('P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
('P0004', 'Siti', 'Jl. Durian 15', 'Solo', 'W'),
('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
('P0008', 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Yogya', 'W')

INSERT INTO tblGaji(kd_pengarang, nama, gaji)
VALUES
('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)


SELECT * FROM tblGaji order by gaji
SELECT * FROM tblPengarang

---Soal 1---
select count(kd_Pengarang) as JumlahPengarang from tblPengarang 

---Soal 2---
select kelamin, count(kelamin) as JumlahPengarang from tblPengarang 
group by kelamin

---Soal 3---
select kota, count(kota) as JumlahKota from tblPengarang
group by kota

---Soal 4---
select kota, count(kota) as jmlKota from tblPengarang
group by kota
having count(kota) > 1

---Soal 5---
select max(kd_Pengarang) as KDTerbesar, min(kd_Pengarang) as KDTerkecil from tblPengarang

select top 1 kd_Pengarang as Tertinggi, kd_Pengarang as Terendah from tblPengarang order by Tertinggi desc, Terendah asc

---Soal 6---
select max(gaji) as GajiTertinggi, min(gaji) as GajiTerendah
from tblGaji

---Soal 7---
select gaji 
from tblGaji 
where gaji > 600000

---Soal 8---
select sum(gaji) as TotalGajiPengarang 
from tblGaji

---Soal 9---
select pg.kota, sum(gj.gaji) as JumlahGaji 
from tblPengarang as pg
join tblGaji as gj ON pg.kd_Pengarang = gj.kd_pengarang
group by pg.kota

---Soal 10---
select * from tblPengarang 
where kd_Pengarang BETWEEN 'P0003' AND 'P0006'

select * from tblPengarang 
where kd_Pengarang >= 'P0003' AND kd_Pengarang <='P0006'

---Soal 11---
select * from tblPengarang 
where kota = 'solo' OR kota = 'magelang' OR kota = 'yogya'

select * from tblPengarang
where kota in ('solo', 'magelang', 'yogya')

---Soal 12---
select * from tblPengarang where not kota = 'yogya'

---Soal 13---
select * from tblPengarang where nama like 'a%'

select * from tblPengarang where nama like '%i'
select * from tblPengarang where nama like '%i' AND kd_Pengarang not like '%4'

select * from tblPengarang where nama like '__a%'

select * from tblPengarang where not nama like '%n'

---Soal 14---
select * from tblPengarang as pg 
join tblGaji as gj ON pg.kd_Pengarang = gj.kd_pengarang

---Soal 15---
select pg.kota, gj.gaji from tblPengarang as pg 
join tblGaji as gj ON pg.kd_Pengarang = gj.kd_pengarang
where gaji < 1000000

---Soal 16---
ALTER TABLE tblPengarang ALTER COLUMN kelamin VARCHAR(10)

---Soal 17---
ALTER TABLE tblPengarang ADD gelar VARCHAR(12)

---Soal 18---
UPDATE tblPengarang SET kota = 'Pekan Baru', alamat = 'Jl. Cendrawasih 65' where nama = 'rian'

---Soal 19---
CREATE VIEW vwPengarang
as select pg.kd_Pengarang, pg.nama, pg.kota, gj.gaji
from tblPengarang as pg
join tblGaji as gj on pg.kd_Pengarang = gj.kd_pengarang

select * from vwPengarang