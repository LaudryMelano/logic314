--- SQL DAY 02 ---

---- CAST -----
SELECT CAST(10 as decimal(18,4))
SELECT CAST('10' as int)
SELECT CAST('2023-03-16' AS date)
SELECT CAST(10.65 as int)

---- CONVERT -----
SELECT CONVERT(decimal(18,4), 10)
SELECT CONVERT(int, '10')
SELECT CONVERT(date, '2023-03-16')
SELECT CONVERT(int, 10.65)

----- DATE TIME ----
SELECT GETDATE(), GETUTCDATE()
SELECT DAY(GETDATE()) AS HARI, MONTH(GETDATE()) AS BULAN, YEAR(GETDATE()) AS TAHUN 

----- DATEADD -----
SELECT DATEADD(year, 2, GETDATE())
SELECT DATEADD(month, 3, GETDATE())
SELECT DATEADD(day, 5, GETDATE())

---- DATEDIFF -----
SELECT DATEDIFF(DAY, '2023-03-16', '2023-03-25'),
	   DATEDIFF(MONTH, '2023-03-16', '2024-06-16'),
	   DATEDIFF(YEAR, '2023-03-16', '2027-06-13')

---- SUB QUERY ----
SELECT name, addrress, email, panjang FROM mahasiswa
WHERE panjang = (SELECT MAX(panjang) FROM mahasiswa)

INSERT INTO  mahasiswa
SELECT name, addrress, email, panjang FROM mahasiswa

---- CREATE VIEW ----

CREATE VIEW vwMahasiswa2
AS
SELECT * FROM mahasiswa

---- SELECT VIEW ----
SELECT * FROM vwMahasiswa2

---- DELETE VIEW ----
DROP VIEW vwMahasiswa2


---- CREATE INDEX ----
CREATE INDEX index_name
ON mahasiswa (name)

CREATE INDEX index_email_addrresss
ON mahasiswa(addrress,email)


----- CREATE UNIQUE INDEX -----
CREATE UNIQUE INDEX unique_index_panjang
ON mahasiswa (panjang)


----- DROP INDEX -----
DROP INDEX index_email_addrresss ON mahasiswa

----- DROP UNIQUE INDEX -----
DROP INDEX unique_index_panjang ON mahasiswa


----- ADD PRIMARY KEY ------
ALTER TABLE mahasiswa 
ADD CONSTRAINT pk_id_address primary key(id,addrress)

----- DROP PRIMARY KEY ------
ALTER TABLE mahasiswa
DROP CONSTRAINT PK__mahasisw__3213E83F09DB7D62


---- UNIQUE CONSTRAINT ------
ALTER TABLE mahasiswa 
ADD CONSTRAINT unique_panjang UNIQUE (panjang)
---unique bisa bernilai Null

UPDATE mahasiswa SET panjang=null where id=5

---- DROP CONSTRAINT ------
ALTER TABLE mahasiswa
DROP CONSTRAINT uniuqe_addrress