﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    public class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Lari...");
        }
    }

    public class Kucing : Mamalia
    {
    }

    public class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang....");
        }
    }

    internal class Program
    {
        static List<User> listUsers = new List<User>();
        static void Main(string[] args)
        {
            //listUser();

            //bool loop = true;
            //while (loop)
            //{
            //    listUserInsert();
            //}

            //dateTime();
            stringToDateTime();
            //timeSpan();
            //overriding();
        }

        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();
        }

        static void inheretance()
        {
            TypeMobil mobil = new TypeMobil();
            mobil.kecepatan = 100;
            mobil.Civic();
        }

        static void timeSpan()
        {
            Console.WriteLine("=========== SPAN DATE TIME ===========");
            Console.WriteLine();

            DateTime date1 = new DateTime(2023, 1,  1, 0, 0, 0);
            DateTime date2 = DateTime.Now;

            //TimeSpan span = date1.Subtract(date2);
            TimeSpan span = date2 - date1;

            Console.WriteLine("No of Days: " + span.Days);
            Console.WriteLine("Total Days : " + span.TotalDays);
            Console.WriteLine("No of Time : " + span.Hours);
            Console.WriteLine("Total Time : " + span.TotalHours);
            Console.WriteLine("No of Minute : " + span.Minutes);
            Console.WriteLine("Total Minute : " + span.TotalMinutes);
            Console.WriteLine("No of Second : " + span.Seconds);
            Console.WriteLine("Total Second : " + span.TotalSeconds);

        }

        static void stringToDateTime() 
        {
            Console.WriteLine("=========== STRING TO DATE TIME ===========");
            Console.WriteLine();

            Console.Write("Masukkan Tanggal (dd/mm/yyyy) : ");
            string sDate = Console.ReadLine();

            DateTime date = DateTime.Parse(sDate);



            Console.WriteLine();
            Console.WriteLine($"Tanggal\t: {date.Day}");
            Console.WriteLine($"Bulan\t: {date.Month}");
            Console.WriteLine($"Tahun\t: {date.Year}");
            Console.WriteLine($"Hari ke-{(int)date.DayOfWeek}");
            Console.WriteLine($"Hari ke-{date.DayOfWeek}");
            Console.WriteLine($"Hari ke-{date.DayOfYear}");

        }

        static void dateTime()
        {

            Console.WriteLine("=========== DATE TIME ===========");
            Console.WriteLine();

            //assigns default value 01/01/0001
            DateTime dt = new DateTime();
            Console.WriteLine(dt);

            //assign datetime now
            DateTime dtNow = DateTime.Now;
            Console.WriteLine(dtNow);

            //assign date without time
            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 3, 9);    
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 30);
            Console.WriteLine(dt6);

        }

        static void listUserInsert() 
        {
       
            Console.WriteLine("=========== LIST INSERT USER CLASS ===========");
            Console.WriteLine();

          
            //{
            //    new User() {nama = "Firdha", umur = 24, alamat = "Tangsel"},
            //    new User() {nama = "Isni", umur = 22, alamat = "Cimahi"},
            //    new User() {nama = "Asti", umur = 23, alamat = "Garut"},
            //    new User() {nama = "Muafa", umur = 22,  alamat = "Bogor"},
            //    new User() {nama = "Toni", umur = 24, alamat = "Garut"},
            //};

            //foreach (var item in listUsers)
            //{
            //    Console.WriteLine(item.nama + " sudah berumur " + item.umur + " dan tinggal di " + item.alamat);
            //}

            Console.WriteLine();

            Console.Write("Masukkan nama : ");
            string nama = Console.ReadLine();

            Console.Write("Masukkan umur : ");
            int umur = int.Parse(Console.ReadLine());

            Console.Write("Masukkan alamat : ");
            string alamat = (Console.ReadLine());

            User user = new User();
            user.nama = nama;
            user.umur = umur;
            user.alamat = alamat;

            listUsers.Add(user);

            Console.WriteLine();
            foreach (var item in listUsers)
            {
                Console.WriteLine(item.nama + " sudah berumur " + item.umur + " dan tinggal di " + item.alamat);
            }

            Console.WriteLine();
        }

        static void listUser()
        {
            Console.WriteLine("=========== LIST CLASS ===========");
            Console.WriteLine();
            List<User> listUser = new List<User>()
            {
                new User() {nama = "Firdha", umur = 24, alamat = "Tangsel"},
                new User() {nama = "Isni", umur = 22, alamat = "Cimahi"},
                new User() {nama = "Asti", umur = 23, alamat = "Garut"},
                new User() {nama = "Muafa", umur = 22,  alamat = "Bogor"},
                new User() {nama = "Toni", umur = 24, alamat = "Garut"},
            };

            foreach (var item in listUser)
            {
                Console.WriteLine(item.nama + " sudah berumur " + item.umur + " dan tinggal di " + item.alamat);
            }

            Console.WriteLine();
            Console.WriteLine("=========== LIST CLASS ===========");
        }
    }
}
