﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic07
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platNo;

        public void utama()
        {
            Console.WriteLine("START");
            Console.WriteLine("Nama\t: " + nama);
            Console.WriteLine("Plat No\t: " + platNo);
            Console.WriteLine("Bensin\t: " + bensin);
            Console.WriteLine("Kecapatan\t: " + kecepatan);
            Console.WriteLine("Posisi\t: " + posisi);

        }
    }

}
