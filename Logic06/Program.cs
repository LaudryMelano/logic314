﻿using System;
using System.Collections.Generic;

namespace Logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Class();
            //listClass();
            //listRemove();   
            listAdd();
        }

        static void listRemove()
        {
            Console.WriteLine("======== LIST REMOVE =========");
            List<User> listUser = new List<User>()
            {
                new User() {Name = "Isni Dwitiniardi", Age = 22},
                new User() {Name = "Astika", Age = 23},
                new User() {Name = "Alfi Azizi", Age = 21}
            };

            listUser.Add(new User() { Name = "Ahmad Anwar", Age = 24 });
            listUser.RemoveAt(0);

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age + "Tahun");
            }

        }

        static void listAdd()
        {
            Console.WriteLine("======== LIST ADD =========");
            List<User> listUser = new List<User>()
            {
                new User() {Name = "Isni Dwitiniardi", Age = 22},
                new User() {Name = "Astika", Age = 23},
                new User() {Name = "Alfi Azizi", Age = 21}
            };

            Console.Write("Masukkan nama : ");
            string nama = Console.ReadLine();

            Console.Write("Masukkan umur : ");
            int umur = int.Parse(Console.ReadLine());

            listUser.Add(new User() { Name = nama, Age = umur});

            listUser.Add(new User() { Name = "Ahmad Anwar", Age = 24 });

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age + "Tahun");
            }
        }

        static void listClass()
        {
            Console.WriteLine("======== LIST CLASS =========");
            List<User> listUser = new List<User>()
            {
                new User() {Name = "Isni Dwitiniardi", Age = 22},
                new User() {Name = "Astika", Age = 23},
                new User() {Name = "Alfi Azizi", Age = 21}
            }; 

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine(listUser[i].Name + " sudah berumur " + listUser[i].Age + "Tahun");
            }

        }

        static void Class()
        {
            Console.WriteLine("======== ClASS =========");
            Mobil mobil2 = new Mobil("RI 1");

            mobil2.isiBensin(12);
            mobil2.percepat();
            mobil2.maju();

            string platNo = mobil2.getPlatNo();

            Console.WriteLine($"Plat Nomor  : {platNo}");
            Console.WriteLine($"Bensin      : {mobil2.bensin}");
            Console.WriteLine($"Kecepatan   : {mobil2.kecepatan}");
            Console.WriteLine($"Posisi      : {mobil2.posisi}");

        }

        static void List()
        {
            Console.WriteLine("======= LIST C# =======");

            List<string> list = new List<string>()
            {
                "Astika",
                "Marcell",
                "Alwi",
                "Toni"
            };

            Console.WriteLine(String.Join(",", list));
        }
    }
}
