﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic06
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        private string nama;
        private string platNo;

        //Constructor//

        public Mobil(string _platNo) 
        {
            platNo = _platNo;
        }

        public string getPlatNo()
        {
            return platNo;
        }

        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin = bensin;
        }
    }
}
