﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Logic07_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal2();
            //soal3();
            //soal5();
            //soal4();
           // soal6();
            //soal7();
            soal8();
            //soal9();
            //soal10();
            //soal11();
            //soal12();
            Console.ReadKey();
        }

        static void soal1()
        {
            Console.WriteLine("======= SOAL 1 FAKTORIAL =======");
            Console.Write("Masukkan jumlah anak : ");
            int anak = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int counter = 1;

            string tampung = "";

            for (int i = anak; i > 0; i--)
            {
                counter *= i;
                tampung += i;

                if (i - 1 > 0)
                tampung += "x";

                //Ternary
                //tampung += tampung == ? i.ToString() : "x" + i.ToString;
            }

            Console.WriteLine($"{anak}! = {tampung} = {counter}");
            Console.WriteLine("");
            Console.WriteLine($"Tersedia {counter} cara untuk duduk");
        }

        static void soal2()
        {
            Console.WriteLine("======= SOAL 2 SINYAL SOS =======");
            Console.Write("Masukkan Signal : ");
            char[] sinyal = Console.ReadLine().ToUpper().ToCharArray();


            int count = 0;
            int countB = 0;

            string tmp = "";

            if (sinyal.Length % 3 != 0)
            {
                Console.WriteLine("Invalid! masukan kode sinyal yang benar");
            }
            else
            {
                for (int i = 0; i < sinyal.Length; i += 3)
                {
                    //if (sinyal[i] == 'S' && sinyal[i + 1] == 'O' && sinyal[i + 2] == 'S')
                    //{

                    //}

                    if(sinyal[i] != 'S' || sinyal[i+1] != 'O' || sinyal[i+2] != 'S')
                    {
                        count++;
                        tmp += "SOS";
                    }
                    else
                    {
                        tmp += sinyal[i].ToString() + sinyal[i+1].ToString() + sinyal[i+2].ToString();
                        countB++;
                    }




                    //if (sinyal[i] == 'S')
                    //{
                    //    if (sinyal[i + 1] == 'O')
                    //    {
                    //        if (sinyal[i + 2] == 'S')
                    //        {

                    //        } else
                    //        {
                    //            count++;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        count++;
                    //    }
                    //}
                    //else
                    //{
                    //    count++;
                    //}
                }
                Console.WriteLine();
                Console.WriteLine($"Total Sinyal Salah adalah : {count}");
                Console.WriteLine($"Total Sinyal Benar adalah : {countB}");
                Console.WriteLine($"Sinyal yang diterima : {String.Join("", sinyal)}");
                Console.WriteLine($"Sinyal yang benar    : {tmp}");
            }
   

           

        }

        static void soal3() 
        {
            Console.WriteLine("\t\t========== PERPUSTAKAAN ===========");
            Console.WriteLine();

            int denda = 500;

            Console.Write("Masukkan Tanggal Peminjaman Buku (dd/mm/yyyy atau dd-mm-yyyy)    : ");
            DateTime date1 = DateTime.Parse(Console.ReadLine());

            Console.Write("Masukkan Tanggal Pengembalian Buku (dd/mm/yyyy atau dd-mm-yyyy)  : ");
            DateTime date2 = DateTime.Parse(Console.ReadLine());

            TimeSpan tempo = date2- date1;

            int result = denda * (tempo.Days-3);

            Console.WriteLine();
            Console.WriteLine($"Denda yang harus dibayarkan Mono adalah {result}");
            
        }

        static void soal4()
        {
            Console.WriteLine("\t============ CEK TANGGAL UJIAN ============");
            Console.WriteLine();

            Console.Write("\tMasukkan Tanggal mulai (dd/mm/yyyy atau dd-mm-yyyy) : ");
            DateTime dateStart = DateTime.Parse(Console.ReadLine());

            Console.Write("\tMasukkan berapa hari lagi akan ujian : ");
            int exam = int.Parse(Console.ReadLine());

            Console.Write("\tMasukkan Tanggal libur  : ");
            int[] holiday = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            //DateTime[] dateExam = new DateTime[exam];

            DateTime tanggalSelesai = dateStart;

            for (int i = 0; i < exam; i++)
            {
                if (i > 0)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(1);
                }
                if(tanggalSelesai.DayOfWeek == DayOfWeek.Saturday || tanggalSelesai.DayOfWeek == DayOfWeek.Sunday)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(2);
                }

                for (int j = 0; j < holiday.Length; j++)
                {
                    if (tanggalSelesai.Day == holiday[j])
                    {
                        tanggalSelesai = tanggalSelesai.AddDays(1);

                        if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            tanggalSelesai = tanggalSelesai.AddDays(2);
                        }
                    }

                }
            }

            /*for (int i = 0; i < exam + holiday.Length; i++)
            {
                if (i == 0)
                {
                    dateExam[i] = dateStart;
                }
                else if (i == exam - 1)
                {
                    dateExam[i] = dateExam[i - 1].AddDays(holiday.Length);
                }
                else if ((dateExam[i - 1].DayOfWeek == DayOfWeek.Friday))
                {
                    dateExam[i] = dateExam[i - 1].AddDays(3);
                }
                else
                {
                    dateExam[i] = dateExam[i - 1].AddDays(1);
                }
            }

            foreach (DateTime dateTime in dateExam)
            {
                Console.WriteLine(dateTime.ToString());
            }*/
            DateTime tanggalUjian = tanggalSelesai.AddDays(1);
            if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
            {
                tanggalUjian = tanggalUjian.AddDays(2);
            }
            Console.WriteLine();
            Console.WriteLine("\tKelas akan ujian pada Tanggal : " + tanggalUjian.ToString("dddd, dd/MMMM/yyyy"));

        }

        static void soal5()
        {
            Console.WriteLine("======== KALIMAT VOKAL & KONSONAN ========");
            Console.WriteLine();
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            int countV = 0;
            int countK = 0;

            foreach (char huruf in kalimat)
            {
                if(huruf >= 'a' && huruf <= 'z')
                {
                    if (huruf == 'a' || huruf == 'i' || huruf == 'u' || huruf == 'e' || huruf == '0')
                    {
                        countV++;
                    }
                    else
                    {
                        countK++;
                    }
                }
            }
            //string[] kalimat = Console.ReadLine().ToLower().Split(" ");



            //for (int i = 0; i < kalimat.Length;i++)
            //{
            //    for (int j = 0; j < kalimat[i].Length ; j++)
            //    {
            //        if (kalimat[i][j] == 'a' || kalimat[i][j] == 'i' || kalimat[i][j] == 'u' || kalimat[i][j] == 'e' || kalimat[i][j] == 'o')
            //        {
            //            countV++;
            //        }
            //        else
            //        {
            //            countK++;
            //        }
            //    }
            //}

            Console.WriteLine($"Jumlah Huruf Vokal dalam kalimat diatas : {countV}");
            Console.WriteLine($"Jumlah Huruf Konsonan dalam kalimat diatas : {countK}");
        }

        static void soal6()
        {
            Console.WriteLine("======== KARAKTER NAMA ANDA ========");
            Console.WriteLine();
            Console.Write("Masukkan Nama Anda : ");
            char[] nama = Console.ReadLine().ToLower().Replace(" ", "").ToCharArray();

            Console.Write("Masukkan banyaknya Bintang : ");
            int b = int.Parse(Console.ReadLine());

            string tmp = default;

            for (int i = 0; i < nama.Length; i++)
            {
                for (int j = 0; j <= b+b; j++)
                {
                    if (j == b)
                    { 
                      tmp += nama[i].ToString();
                    }
                    else
                    {
                        tmp += "*";
                    }
                }
                tmp += "\n";
            }

            //for (int i = 0; i < nama.Length; i++)
            //{
            //    for (int j = 0; j <= 6; j++)
            //    {
            //        if (j == 3)
            //        {
            //            tmp += nama[i].ToString();
            //        }
            //        else
            //        {
            //            tmp += "*";
            //        }
            //    }
            //    tmp += "\n";
            //}

            Console.WriteLine($"Pengejaan nama Anda adalah : \n\n{tmp}");
        }

        static void soal7()
        {
            Console.WriteLine("\t============ SELAMAT DATANG DI THE COST ============");
            Console.WriteLine();

            int totalHarga = 0;

            Console.Write("\tMasukkan total menu yang dipesan : ");
            int menu = int.Parse(Console.ReadLine());

            Console.WriteLine();
            loop:
            Console.Write("\tMasukkan harga tiap menu : ");
            int[] harga = new int[menu-1];
            harga = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine();
            if (harga.Length > menu) 
            {
                Console.WriteLine("Masukkan total menu yang sesuai");
                goto loop;
            }
            else
            {
                Console.Write("\tMasukkan no makanan alergi : ");
                int alergi = int.Parse(Console.ReadLine());

                Console.WriteLine();

                Console.Write("\tSplit Bill dengan berapa orang : ");
                int split = int.Parse(Console.ReadLine());

                Console.WriteLine();

                for (int i = 0; i < harga.Length; i++)
                {
                    if (alergi != i + 1)
                    {
                        totalHarga += harga[i];
                    }
                    else
                    {
                        continue;
                    }
                }

                Console.WriteLine();

                Console.WriteLine("\tTotal Harga yang harus dibayar adalah : " + (totalHarga / split).ToString("Rp. #,##0.00"));

                Console.Write("\tMasukkan Jumlah uang Anda : Rp.");
                int bayar = int.Parse(Console.ReadLine());

                Console.WriteLine();

                int kembalian = bayar - (totalHarga / split);

                if (kembalian == 0)
                    Console.WriteLine("\tTerimakasih Uang Anda Pas");
                else if (kembalian > 0)
                    Console.WriteLine("\tKembalian uang Anda : " + kembalian.ToString("Rp. #,##0.0"));
                else
                    Console.WriteLine("\tUang Kurang, Anda harus menambah : " + "Rp." + kembalian.ToString("- #,##0.0"));
            }

           
        }

        static void soal8()
        {
            Console.WriteLine("====== STAR CASE =======");
            Console.Write("Masukkan jumlah bintang : ");
            int bintang = int.Parse( Console.ReadLine() );
            //int b = bintang;

            for (int i = 0; i < bintang; i++)
            {
                for (int j = 0; j < bintang; j++)
                {
                    if (i >= j)
                    {
                        Console.Write('*');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }

            for (int i = 0; i < bintang; i++)
            {
                for (int j = 0; j < bintang; j++)
                {
                    //if (j < bintang - 1 - i)
                    //{
                    //    Console.Write('-');
                    //}
                    //else
                    //{
                    //    Console.Write('*');
                    //}
                    if(bintang-1-i==j||i == bintang-1||j == bintang - 1)
                        Console.Write('*');
                    else 
                        Console.Write(" "); 

                }
                Console.WriteLine();
            }

            //for (int i = 0; i < bintang; i++)
            //{
            //    for (int j = 0; j < b; j++)
            //    {
            //        Console.Write(' ');
            //    }
            //    b--;
            //    for (int j = 0; j <= i; j++)
            //    {
            //        Console.Write('*');
            //    }
            //    Console.WriteLine();
            //}
        }

        static void soal9()
        {
            Console.WriteLine("\t=========== PENJUMLAHAN DIAGONAL MATRIKS =============");
            Console.WriteLine();

            int[,] diagonal = new int[3,3];

            int sum1 = 0;
            int sum2 = 0;

            Console.WriteLine("\tMasukkan elemen didalam  matrix : ");
            for (int i = 0; i < diagonal.GetLength(0); i++)
            {
                for (int j = 0; j < diagonal.GetLength(1); j++)
                {
                    Console.Write("\telement - [{0}],[{1}] : ", i, j);
                    diagonal[i, j] = int.Parse(Console.ReadLine());
                }
            }

            string tmp1 = "";
            string tmp2 = "";

            for (int i = 0;i < diagonal.GetLength(0); i++)
            {
                for (int j = 0;j < diagonal.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        sum1 += diagonal[i,j];
                        if (tmp1 == "")
                            tmp1 += diagonal[i, j].ToString();
                        else if (diagonal[i,j] < 0)
                            tmp1 += "-"+Math.Abs(diagonal[i, j]).ToString();
                        else
                            tmp1 += "+"+ diagonal[i,j].ToString();
                    }

                    if(i == diagonal.GetLength(1)-1-j)
                    {
                        sum2 += diagonal[i,j];
                        if (tmp2 == "")
                            tmp2 += diagonal[i, j].ToString();
                        else if (diagonal[i, j] < 0)
                            tmp2 += "-" + Math.Abs(diagonal[i, j]).ToString();
                        else
                            tmp2 += "+" + diagonal[i, j].ToString();
                    }
                }
            }
            Console.WriteLine("\tJumlah angka diagonal pertama  : " + tmp1 + " = " +sum1);
            Console.WriteLine("\tJumlah angka diagonal kedua    : " + tmp2 + " = " +sum2);

            Console.WriteLine("\tJumlah perbedaan diagonal      : " + (sum1-sum2));
            
        }

        static void soal10()
        {
            Console.WriteLine("======== CANDLE HAPPY BIRTHDAY ========");
            Console.WriteLine();

            Console.Write("Masukkan tinggi lilin : ");
            int[] lilin = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int count = 0;
            int max = 0;

            for (int i = 0; i < lilin.Length; i++)
            {
                if (lilin[i] > max)
                    max = lilin[i];
                

                if (lilin[i] == max)
                    count++;
            }

            Console.WriteLine();
            Console.WriteLine("Angka lilin yang Tertinggi                : " + max);
            Console.WriteLine("Jumlah lilin yang bisa di Tiup oleh Adik  : " + count);
        }

        static void soal11()
        {
            Console.WriteLine("======== HEAVY ROTATION ========");
            Console.WriteLine();

            Console.Write("Masukkan deret angka : ");
            int[] deret = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.Write("Masukkan rotation : ");
            int rot = int.Parse(Console.ReadLine());

            int tmp = 0;

            Console.WriteLine();

            for (int i = 0; i < rot; i++)
            {
                for (int j = 0; j < deret.Length; j++)
                {
                    if (j == 0)
                    {
                        tmp = deret[j];
                    }
                    else if (j == deret.Length - 1)
                    {
                        deret[j - 1] = deret[j];
                        deret[deret.Length - 1] = tmp;
                    }
                    else
                    {
                        deret[j - 1] = deret[j];
                    }
                }
            }

            foreach (int item in deret)
            {
                Console.Write("Hasil setelah di rotasi : " + item);
            }

            Console.WriteLine(String.Join(" ", deret));
        }

        static void soal12()
        {
            Console.WriteLine("====== SORTING CASE =======");
            Console.WriteLine();
            Console.Write("Masukkan banyak deret : ");
            int deret = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int[] angka = new int[deret];
            int x = 0;
            int no = 1;

            while (x < deret)
            {
                Console.Write($"Masukkan data ke-{no++} : ");
                angka[x] = int.Parse(Console.ReadLine());
                x++;
            }
            Console.WriteLine();

            int tmp = 0;

            for (int i = 0; i < angka.Length; i++)
            {
                for (int j = 0; j < angka.Length-1; j++)

                    if (angka[j] > angka[j + 1])
                    {
                            tmp = angka[j];
                            angka[j] = angka[j + 1];
                            angka[j + 1] = tmp;
                    }
            }

            foreach (int item in angka)
            {
                Console.Write("Hasil Sorting : "+item);
            }
           
        }

    }
}
