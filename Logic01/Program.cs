﻿using System;
using System.Runtime.InteropServices;

namespace Logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Convertion();
            Operator();
            //Modulus();
            //operatorPenugasan();
            //operatorPerbandingan();
            //operatorLogika();
            //Hitung();


        }

        

        static void Hitung()
        {
            int mangga, apel;

            Console.Write("Masukkan Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            int hasil = Penjumlahan(mangga, apel);

            Console.WriteLine($"Total buah adalah : {hasil}");

        }

        static int Penjumlahan (int x, int y) 
        {
            int hasil = x + y;

            return hasil;   
        }

        static int Pengurangan (int x, int y) 
        {
            int hasil = x + y;

            return hasil;
        }

        static void operatorLogika() 
        {
            Console.WriteLine("===Operator Logika===");
            Console.Write("Masukkan umur : ");
            int umur = int.Parse(Console.ReadLine());

            Console.Write("Massukan Password : ");
            string password = Console.ReadLine();

            bool isAdult = umur > 18;
            bool isPasswordValid = password == "Admin";

            if (isAdult && isPasswordValid ) { 
                Console.Write("Welcome, To the club!");
            }
            else if (isAdult && !isPasswordValid)
            {
                Console.WriteLine("Anda sudah dewasa dan password salah");
            }
            else if(!isAdult && isPasswordValid){
                Console.WriteLine("Anda belum dewasa dan password benar");
            }
            else
            {
                Console.Write("Sorry, Try Again");
            }

        }

        static void operatorPerbandingan()
        {
            int mangga, apel;

            Console.WriteLine("===Operator Perbandingan===");

            Console.Write("Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());

            Console.Write("Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("===Hasil Perbandingan===");
            Console.WriteLine($"Mangga < Apel = {mangga < apel} ");
            Console.WriteLine($"Mangga > Apel = {mangga > apel} ");
            Console.WriteLine($"Mangga <= Apel = {mangga <= apel}");
            Console.WriteLine($"Mangga >= Apel = {mangga >= apel}");
            Console.WriteLine($"Mangga = Apel = {mangga == apel}");
            Console.WriteLine($"Mangga != Apel = {mangga != apel}");
        }

        static void operatorPenugasan() 
        {
            int mangga = 10;
            int apel = 8;

            //diinisiasi ulang
            mangga = 15;

            Console.WriteLine("===Operator Penugasan====");
            Console.WriteLine($"Mangga = {mangga}");
            Console.WriteLine($"Apel = {apel}");

            apel += mangga;
            Console.WriteLine($"Apel += {apel}");

            mangga = 15;
            apel = 8;
            apel -= mangga;
            Console.WriteLine($"Apel -= {apel}");

            mangga = 15;
            apel = 8;
            apel *= mangga;
            Console.WriteLine($"Apel *= {apel}");

            mangga = 15;
            apel = 8;
            apel /= mangga;
            Console.WriteLine($"Apel /= {apel}");

            mangga = 15;
            apel = 8;
            apel %= mangga;
            Console.WriteLine($"Apel %= {apel}");

        }

        static void Modulus()
        {
            Console.WriteLine("====Modulus====");
            int mangga, apel, orang, mod, hasil = 0;

            Console.Write("Masukkan Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());


            hasil = mangga + apel;


            Console.WriteLine("Total Buah yang tersedia = {0}", hasil);

            Console.Write("Masukkan Jumlah Orang = ");
            orang = int.Parse(Console.ReadLine());

            mod = hasil % orang;

            Console.WriteLine("Sisa buah-buahan yang tersedia = " + mod);

        }

        static void Operator()
        {
            Console.WriteLine("OpertorAritmatika");
            int mangga, apel, hasil = 0;

            Console.Write("Masukkan Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            hasil = apel / mangga;

            Console.WriteLine($"Total buah adalah : {hasil}");
        }

        static void Convertion()
        {
            Console.WriteLine("===Konversi===");
            int umur = 23;
            string newUmur = umur.ToString();
            Console.WriteLine("Umur : " + newUmur);

            int myInt = 10;
            double myDouble = 20.5;
            bool myBool = false;

            string myString = Convert.ToString(myInt);
            double myConDouble = Convert.ToDouble(myInt);
            int myConInt = Convert.ToInt32(myDouble);
            string myConString = Convert.ToString(myBool);

            Console.WriteLine("Convert int to string : " + myString);
            Console.WriteLine("Convert int to double : " + myConDouble);
            Console.WriteLine("Convert double to int : " + myConInt);
            Console.WriteLine("Convert bool to string : " + myConString);
        }

    }
}
