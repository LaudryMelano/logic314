﻿using System;
using System.Globalization;
using System.Linq;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CallMenu();
            //Soal5_v2();
            //Soal12();

        }

        static void CallMenu()
        {
            bool loop = true;

            while (loop)
            {
                Console.Clear();
                Menu();
                Console.WriteLine();
                Console.Write("Ulangi proses ? (y/n) ");
                string input = Console.ReadLine().ToLower();

                if (input == "y")
                {
                    Console.Clear();
                    Menu();

                    Console.WriteLine();
                    Console.Write("Ulangi proses ? (y/n) ");
                    input = Console.ReadLine().ToLower();

                    Console.WriteLine();
                    if (input == "n")
                    {
                        loop = false;
                        Console.WriteLine("Terimakasih telah menggunakan Aplikasi kami");
                    }
                }
                else if (input == "n")
                {
                    loop = false;
                    Console.WriteLine("Terimakasih telah menggunakan Aplikasi kami");
                }
                else
                {
                    Console.WriteLine("Pilih Y/N!");
                }
            }

        }

        static void Menu()
        {
            Console.WriteLine("==== TUGAS LOGIC ====");
            Console.WriteLine("1. Upah Karyawan");
            Console.WriteLine("2. Split Kalimat");
            Console.WriteLine("3. Manipulasi String Replace v.1");
            Console.WriteLine("4. Manipulasi String Replace v.2");
            Console.WriteLine("5. Manipulasi String Remove");
            Console.WriteLine("6. Perkalian 3");
            Console.WriteLine("7. Penjumlahan 5");
            Console.WriteLine("8. Fibonacci");
            Console.WriteLine("9. Konversi Jam");
            Console.WriteLine("10. IMP Fashion");
            Console.WriteLine("11. Belanja Lebaran");
            Console.WriteLine("12. Deret Angka");
            Console.WriteLine("====================");

            Console.Write("Pilih Menu : ");
            string option = Console.ReadLine();

            Console.WriteLine();

            switch (option)
            {
                case "1":
                    Soal1();
                    break;
                case "2":
                    Soal2();
                    break;
                case "3":
                    Soal3();
                    break;
                case "4":
                    Soal4();
                    break;
                case "5":
                    Soal5();
                    break;
                case "6":
                    Soal6();
                    break;
                case "7":
                    Soal7();
                    break;
                case "8":
                    Soal8();
                    break;
                case "9":
                    Soal9();
                    break;
                case "10":
                    Soal10();
                    break;
                case "11":
                    Soal11();
                    break;
                case "12":
                    Soal12();
                    break;
                default:
                    Console.WriteLine("Pilih No sesuai daftar!");
                    break;
            }
        }

        static void Soal1()
        {
            Console.WriteLine("         ===== APLIKASI MENGHITUNG UPAH KARYAWAN =====");
            Console.WriteLine();

            double jamKerja, upah = 0, lembur = 0, totalGaji = 0;
            int golongan;
            int g1 = 2000;
            int g2 = 3000;
            int g3 = 4000;
            int g4 = 5000;

        loop:
            Console.Write("         Masukkan Golongan  : ");
            golongan = int.Parse(Console.ReadLine());

            Console.Write("         Masukkan Jam Kerja : ");
            jamKerja = double.Parse(Console.ReadLine());

            Console.WriteLine();



            if (golongan == 1 && jamKerja > 0)
            {
                if (jamKerja > 0 && jamKerja <= 40)
                {
                    totalGaji = upah = g1 * jamKerja;
                }
                else 
                {
                    jamKerja -= 40;
                    lembur = 1.5 * g1 * jamKerja;
                    upah = g1 * 40;
                    totalGaji = upah + lembur;
                }
            }
            else if (golongan == 2 && jamKerja > 0)
            {
                if (jamKerja > 0 && jamKerja <= 40)
                {
                    totalGaji = upah = g2 * jamKerja;
                }
                else
                {
                    jamKerja -= 40;
                    lembur = 1.5 * g2 * jamKerja;
                    upah = g2 * 40;
                    totalGaji = upah + lembur;
                }
            }
            else if (golongan == 3 && jamKerja > 0)
            {
                if (jamKerja > 0 && jamKerja <= 40)
                {
                    totalGaji = upah = g3 * jamKerja;
                }
                else 
                {
                    jamKerja -= 40;
                    lembur = 1.5 * g3 * jamKerja;
                    upah = g3 * 40;
                    totalGaji = upah + lembur;
                }
            }
            else if (golongan == 4 && jamKerja > 0)
            {
                if (jamKerja > 0 && jamKerja <= 40)
                {
                    totalGaji = upah = g4 * jamKerja;
                }
                else 
                {
                    jamKerja -= 40;
                    lembur = 1.5 * g4 * jamKerja;
                    upah = g4 * 40;
                    totalGaji = upah + lembur;
                }
            }
            else if (golongan <= 0 || golongan > 4 || jamKerja <= 0)
            {
                Console.WriteLine("Periksa kembali golongan atau jam kerja!");
                Console.WriteLine();
                goto loop;
            }

            Console.WriteLine("\t====== INFORMASI UPAH ======");
            Console.WriteLine($"\tUpah        : {upah}");
            Console.WriteLine($"\tlembur      : {lembur}");
            Console.WriteLine($"\tTotal Gaji  : {totalGaji}");
        }

        static void Soal2()
        {
            Console.WriteLine("         ===== APLIKASI SPLIT KALIMAT =====");
            Console.WriteLine();

            Console.Write("Masukkan Kalimat : ");
            string[] kalimat = Console.ReadLine().Split(" ");

            Console.WriteLine();

            int no = 1;

            foreach (string s in kalimat)
            {
                Console.WriteLine($"Kata ke-{no} : {s}");
                no++;
            }
            Console.WriteLine();
            Console.WriteLine("Total kata : " + kalimat.Length);
        }

        static void Soal3()
        {
            Console.WriteLine("====== MANIPULASI STRING REPLACE  =====");

            Console.Write("Masukkan Kalimat : ");
            string[] kata = Console.ReadLine().Split(" ");


            for (int i = 0; i < kata.Length; i++)
            {
                Console.Write("\n");
                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kata[i][j]);
                    }
                    else if (j == kata[i].Length - 1)
                    {
                        Console.Write(kata[i][j]);
                    }
                    else
                    {
                        Console.Write(kata[i].Replace(kata[i], "*"));
                    }

                }
            }
        }

        static void Soal4()
        {
            Console.WriteLine("====== MANIPULASI STRING REPLACE  =====");

            Console.Write("Masukkan Kalimat : ");
            string[] kata = Console.ReadLine().Split(" ");

            for (int i = 0; i < kata.Length; i++)
            {
                Console.Write(" ");
                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kata[i].Replace(kata[i], "*"));
                    }
                    else if (j == kata[i].Length - 1)
                    {
                        Console.Write(kata[i].Replace(kata[i], "*"));
                    }
                    else
                    {
                        Console.Write(kata[i][j]);
                    }

                }
            }
        }

        static void Soal5_v2()
        {
            Console.WriteLine("====== MANIPULASI STRING REMOVE =====");

            Console.Write("Masukkan Kalimat : ");
            string[] kata = Console.ReadLine().Split(" ");
            string tmp = "";

            for (int i = 0; i < kata.Length; i++)
            {
                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j > 0)
                    {
                        tmp += kata[i][j];
                        
                        if (j == kata[i].Length - 1)
                        {
                            tmp += " ";
                        }
                    }

                }
            }
            Console.WriteLine(tmp);
        }

        static void Soal5()
        {
            Console.WriteLine("====== MANIPULASI STRING REMOVE =====");

            Console.Write("Masukkan Kalimat : ");
            string[] kata = Console.ReadLine().Split(" ");

            for (int i = 0; i < kata.Length; i++)
            {
                Console.Write(" ");
                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kata[i].Remove(0));
                    }
                    else
                    {
                        Console.Write(kata[i][j]);
                    }

                }
            }

        }

        static void Soal6()
        {
            Console.WriteLine("====== PERKALIAN 3 =====");

            Console.Write("Masukkan Angka : ");
            int number = int.Parse(Console.ReadLine());

            Console.WriteLine();

            int n = 3;

            for (int i = 1; i <= number; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write("*");
                }
                else
                {
                    Console.Write(n);
                }
                n *= 3;
                Console.Write(" ");
            }
        }

        static void Soal7()
        {
            Console.WriteLine("====== PENJUMLAHAN 5 =====");

            Console.Write("Masukkan Panjang Deret : ");
            int number = int.Parse(Console.ReadLine());

            Console.WriteLine();

            int n = 5;

            for (int i = 1; i <= number; i++)
            {
                if (i % 2 != 0)
                {

                    Console.Write($"{-n}");
                }
                else
                {
                    Console.Write(n);
                }
                n += 5;
                Console.Write(" ");
            }

        }

        static void Soal8()
        {
            Console.WriteLine("====== FIBONACCI  =====");
            Console.Write("Masukkan Jumlah Angka Fibonacci : ");
            int number = int.Parse(Console.ReadLine());

            Console.WriteLine();

            int n1 = 0, n2 = 1, n3;

            for (int i = 1; i <= number; i++)
            {
                n3 = n1 + n2;
                n2 = n1;
                n1 = n3;
                Console.Write(n3 + ", ");
            }
        }

        static void Soal8_V2()
        {
            
            Console.Write("Masukkan angka Fibbonaci : ");
            int num = int.Parse(Console.ReadLine());

            int[] numArr = new int[num];

            for(int i=0; i<num; i++)
            {
                if (i <= 1)
                {
                    numArr[i] = 1;
                }
                else
                {
                    numArr[i] = numArr[i - 2] + numArr[i-1];
                }
            }
            Console.Write(string.Join(" ", numArr));
        }

        static void Soal9()
        {
            Console.WriteLine("====== KONVERSI JAM  =====");
            Console.Write("Masukkan Pukul Berapa dalam AM/PM (07:05:45PM) : ");
            string waktu = Console.ReadLine().ToUpper();

            string format = waktu.Substring(8, 2);
            int jam = int.Parse(waktu.Substring(0,2));

            if (jam <= 12)
            {
                if (format == "PM")
                {
                    if (jam == 12)
                    {
                        Console.WriteLine(jam.ToString() + format.Substring(2, 6));
                    }
                    else
                    {
                        jam += 12;
                        Console.WriteLine(jam.ToString() + format.Substring(2, 6));
                    }  
                }
                else
                {
                    if (jam == 12)
                    {
                        Console.WriteLine($"00"+waktu.Substring(2, 6));
                    }
                    else
                    {
                        Console.WriteLine(waktu.Substring(0, 8));
                    }
                }
            }
            else
            {
                Console.WriteLine("Masukkan Input yang benar!");
            }
        }

        static void Soal10()
        {
            string merk = default;
            int harga = 0;

            Console.WriteLine("====== IMP FASHION  =====");
            Console.Write("1. IMP ");
            Console.Write("2. PRADA ");
            Console.Write("3. GUCCI ");
            Console.WriteLine("\n\n");

            Console.Write("Masukkan Kode Baju : ");
            string kode = Console.ReadLine();

            Console.Write("Masukkan Kode Baju (S/M/L/XL/XXL) : ");
            string ukuran = Console.ReadLine().ToUpper();

            if (kode == "1")
            {
                merk = "IMP";
                if (ukuran == "S")
                {
                    harga = 200000;
                }
                else if (ukuran == "M")
                {
                    harga = 220000;
                }
                else
                {
                    harga = 250000;
                }
            }
            else if (kode == "2")
            {
                merk = "PRADA";
                if (ukuran == "S")
                {
                    harga = 150000;
                }
                else if (ukuran == "M")
                {
                    harga = 160000;
                }
                else
                {
                    harga = 170000;
                }
            }
            else if (kode == "3")
            {
                merk = "GUCCI";
                if (ukuran == "S")
                {
                    harga = 200000;
                }
                else if (ukuran == "M")
                {
                    harga = 200000;
                }
                else
                {
                    harga = 200000;
                }
            }
            else
            {
                Console.WriteLine("Masukkan Kode Baju yang Sesuai");
            }
            Console.WriteLine();
            Console.WriteLine("========== STRUK BELANJA ==========");
            Console.WriteLine();
            Console.WriteLine($"        Merk Baju   : {merk}");
            Console.WriteLine($"        Harga Baju  : {harga}");
            Console.WriteLine();
            Console.WriteLine("========== TERIMAKASIH ============");
        }

        static void Soal11()
        {
            Console.WriteLine("====== BELANJA LEBARAN  =====");

            Console.Write("Masukkan Jumlah Uang Kamu : ");
            int uang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Harga Baju : ");
            string[] hargaBaju = Console.ReadLine().Split(",");
            int[] hargaBajuInt = Array.ConvertAll(hargaBaju, int.Parse);

            Console.Write("Masukkan Harga Celana : ");
            string[] hargaCelana = Console.ReadLine().Split(",");
            int[] hargaCelanaInt = Array.ConvertAll(hargaCelana, int.Parse);

            int harga;
            int maxBelanja=0;

            for(int i = 0; i < hargaBaju.Length; i++)
            {
                for (int j = 0; j < hargaCelana.Length; j++)
                {
                    harga = hargaBajuInt[i] + hargaCelanaInt[j];
                    if (harga <= uang && harga>=maxBelanja)
                    {
                         maxBelanja=harga;
                    }   
                }
            }

            Console.WriteLine();
            Console.WriteLine("========== BELANJA LEBARAN ==========");
            Console.WriteLine();
            Console.WriteLine($" Pakaian dan Celana yang sesuai Budget   : {maxBelanja}");
            Console.WriteLine();


        }

        static void Soal12()
        {
            Console.WriteLine("====== DERET ANGKA  =====");
            Console.WriteLine();
            Console.Write("Masukkan Jumlah Deret Angka  : ");
            int number = int.Parse(Console.ReadLine());

            Console.WriteLine();

            int n1=1, n2 = number;

            string tmp = "";

            for (int i = 0; i<number; i++)
            {
                for (int j = 0; j < number; j++)
                {
                    if (i == 0)
                    {
                        tmp += n1++;
                    }else if (i == number-1)
                    {
                        tmp += n2--;
                    }else
                    {
                        if (j == 0)
                        {
                            tmp += "*";
                        }else if (j == number - 1)
                        {
                            tmp += "*";
                        }
                        else
                        {
                            tmp += " ";
                        }
                    }
                }
                tmp += "\n";
            }

            Console.WriteLine(tmp);
            //int[] n = new int[number];

                //for (int i = 0; i < number; i++)
                //{
                //    n[i] = i + 1;
                //}

                //for (int i = 0; i < n.Length; i++)
                //{
                //    for (int j = 0; j < n.Length; j++)
                //    {
                //        if(i == 0)
                //        {
                //            Console.Write($" {n[j]} ");
                //        }else if (i == n.Length-1)
                //        {
                //            Console.Write($" {n[(n.Length - 1) - j]} ");
                //        }
                //        else
                //        {
                //            if ( j == 0)
                //            {
                //                Console.Write(" * ");
                //            }else if (j == n.Length - 1)
                //            {
                //                Console.Write(" * ");
                //            }
                //            else
                //            {
                //                Console.Write("   ");
                //            }
                //        }
                //    }
                //    Console.WriteLine();
                //}


                //for (int j = 0; j < n.Length; j++)
                //{
                //    if (j == 0)
                //    {
                //        for (int k = 0; k < n.Length; k++)
                //        {
                //            Console.Write(n[k]);
                //            Console.Write(" ");
                //        }
                //    }
                //    else if (j == n.Length - 1)
                //    {
                //        for (int l = n.Length - 1; l >= 0; l--)
                //        {
                //            Console.Write(n[l]);
                //            Console.Write(" ");
                //        }
                //    }
                //    else
                //    {
                //        for (int m = 0; m < n.Length; m++)
                //        {
                //            if (m == 0)
                //            {
                //                Console.Write("*");
                //                Console.Write(" ");
                //            }
                //            else if (m == n.Length - 1)
                //            {
                //                Console.Write("*");
                //                Console.Write(" ");
                //            }
                //            else
                //            {
                //                Console.Write(" ");
                //                Console.Write(" ");
                //            }
                //        }
                //    }
                //    Console.Write("\n");
                //}


        }
    }
}
