﻿using System;
using System.Globalization;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ForLoopIncrement();
            //ForLoopDecrement();
            //ForLoopBreak();
            //ForLoopContinue();
            //ArrayStatic();
            //ArrayForEach();
            //Array2Dimensi();
            //Array2DimensiFor();
            //SplitandJoin();
            //SubsString();
            //CharrArray();

            //longestWorld();
            pyramidNumber();

        }

        static void pyramidNumber() 
        {
            for (int i = 1; i <= 5; i++) 
            {
                for (int j = i; j<=5; j++)
                {
                    Console.Write(j);
                }
                for (int k = 4; k >= i; k--)
                {
                    Console.Write(k);
                }
                Console.WriteLine();
            }
            for (int l = 4; l >= 1; l--)
            {
                for (int m = l; m <= 5; m++)
                {
                    Console.Write(m);
                }
                for (int n = 4; n >= l; n--)
                {
                    Console.Write(n);
                }
                Console.WriteLine();
            }
        }

        static void longestWorld()
        {
            Console.Write("Masukkan sebuah kalimat : ");
            
            string letter = Console.ReadLine();

            string lWord = LongestWord(letter);

            Console.WriteLine();

            Console.WriteLine($"Kata yang terpanjang dari kalimat tersebut adalah : {lWord}");

        }

        static string LongestWord(string letter)
        {
            int word =0;
            string[] letterS = letter.Split();
           

            for(int i=0; i<letterS.Length; i++)
            {
                if (letterS[i].Length > word)
                {
                    letter = letterS[i];
                    word = letterS[i].Length;
                }
            }
            return letter;           
        }

        static void CharrArray()
        {
            Console.WriteLine("==== CharrArray =====");

            Console.Write("Masukkan String : ");
            string kalimat = Console.ReadLine();

            char[] chars = kalimat.ToCharArray();

            foreach (char c in chars) 
            { 
            Console.Write(c);
            }
        }

        static void SubsString()
        {
            Console.WriteLine("==== SUBSSTRING ====");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring (1,4) : " + kalimat.Substring(1,4));
            Console.WriteLine("Substring (5, 2) : " + kalimat.Substring(5,2));
            Console.WriteLine("Substring (7, 9) : " + kalimat.Substring(7,9));
            Console.WriteLine("Substring (9) : " + kalimat.Substring(9));
        }

        static void SplitandJoin()
        {
            Console.WriteLine("==== Split dan Join ====");

            Console.Write("Masukkan Kalimat : ");
            string[] split = Console.ReadLine().Split(" "); 
            
            foreach (string s in split)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine(string.Join(",", split));
        }

        static void Array2DimensiFor()
        {
            int[,] array = new int[,]
            {
                {1, 2},
                {4, 5},
                {7, 8}
            };

            for (int i = 0; i <=array.Rank; i++) 
            {
                for (int j = 0; j < array.Rank; j++)
                {
                    Console.Write(array[i,j]);
                }
                Console.WriteLine();    
            }   

        }

        static void Array2Dimensi()
        {
            int[,] array = new int[,] 
            {
                {1, 2, 3}, 
                {4, 5, 6},
                {7, 8, 9}
            };

            Console.WriteLine(array[0,1]);
            Console.WriteLine(array[1,2]);
        }

        static void ArrayFor()
        {
           // string[] array = new string[]
           //{
           //     "Abdullah Muafa",
           //     "Isni Dwitiniarti",
           //     "Ilham Rizki",
           //     "Marcellino",
           //     "Alwi Siregar"
           //};

           // for (int i = 0; i < array.Length; i++)
           // {
           //     Console.WriteLine(array[i]);
           // }

            int[] array = new int[]
           {
               1, 2, 3, 4
           };

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == array[0])
                Console.WriteLine(array[i]);
            }

        }

        static void ArrayForEach()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniarti",
                "Ilham Rizki",
                "Marcellino",
                "Alwi Siregar"
            };

            //Mencetak Array menggunakan foreach
            foreach (string item in array)
            {
                Console.WriteLine(item);
            }
        }

        static void ArrayStatic()
        {
            int[] staticArray = new int[3];

            //mengisi array

            staticArray[0] = 1;
            staticArray[1] = 2;
            staticArray[2] = 3;

            //cetak array

            Console.WriteLine(staticArray[0]);
            Console.WriteLine(staticArray[1]);
            Console.WriteLine(staticArray[2]);
        }


            static void NestedFor()
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        Console.Write($"[{i},{j}]");
                    }
                    //Console.WriteLine();
                    Console.Write("\n");
                }

            }

            static void ForLoopContinue()
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i == 5)
                    {
                        continue;
                    }
                    Console.WriteLine(i);
                }
            }

            static void ForLoopBreak()
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i == 5)
                    {
                        break;
                    }
                    Console.WriteLine(i);
                }
            }

            static void ForLoopDecrement()
            {
                for (int i = 10; i > 0; i--)
                {
                    Console.WriteLine(i);
                }
            }

            static void ForLoopIncrement()
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine(i);
                }
            }


            static void DoWhileLoop()
            {
                int a = 0;
                do
                {
                    Console.WriteLine($"{a}");
                    a++;
                } while (a < 5);
            }

            static void WhileLoop()
            {
                Console.WriteLine("=== While Loop ===");

                bool loop = true;

                int nilai = 1;

                while (loop)
                {
                    Console.WriteLine($"Proses ke : {nilai}");

                    Console.Write("Ulangi proses ? (y/n) ");
                    string input = Console.ReadLine().ToLower();

                    if (input == "y")
                    {
                        nilai++;
                    }
                    else if (input == "n")
                    {
                        loop = false;
                    }
                    else
                    {
                        Console.WriteLine("Pilih Y/N!");
                    }
                }
            }

            static void Soal4()
            {
                Console.Write("Masukkan angka = ");
                int angka = int.Parse(Console.ReadLine());

                if (angka % 2 == 0)
                {
                    Console.WriteLine($"Angka {angka} adalah bilangan genap");
                }
                else
                {
                    Console.WriteLine($"Angka {angka} adalah bilangan ganjil");
                }
            }

            static void Soal3()
            {
                Console.Write("Masukkan nilai = ");
                int nilai = int.Parse(Console.ReadLine());

                if (nilai >= 80 && nilai <= 100)
                {
                    Console.WriteLine("A");
                }
                else if (nilai >= 60 && nilai < 80)
                {
                    Console.WriteLine("B");
                }
                else if (nilai >= 0 && nilai < 60)
                {
                    Console.WriteLine("C");
                }
                else
                {
                    Console.WriteLine("Masukkan Nilai yang benar");
                }
            }

            static void Soal2()
            {
                Console.Write("Masukkan jumlah putung rokok = ");
                int pr = int.Parse(Console.ReadLine());

                int harga = 500;
                int br = 0;
                int sp = 0;

                if (pr < 8)
                {
                    Console.WriteLine($"Putung Rokok Anda {pr}");
                }
                else
                {
                    while (pr >= 8)
                    {
                        pr -= 8;
                        sp = pr % 8;
                        br++;
                    }
                    Console.WriteLine($"Berhasil membuat {br} batang rokok dan sisa putung rokok {sp} dan Hasil penjualan sebesar {harga * br}");
                }


            }

            static void Soal1()
            {
                Console.WriteLine("==== Modulus =====");

                Console.Write("Masukkan Angka = ");
                int angka = int.Parse(Console.ReadLine());
                Console.Write("Masukkan Pembagi = ");
                int pembagi = int.Parse(Console.ReadLine());

                int hasil = angka % pembagi;

                Console.WriteLine($"{angka} % {pembagi} adalah {hasil}");
            }

    }
}

