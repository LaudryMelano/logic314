 --- KISI - KISI ASTRA ---

 create database db_kisikisi

 CREATE table assignment(
 id int primary key identity(1,1),
 name varchar(50) not null,
 marks varchar(50) null,
 grade int not null
 )

 INSERT INTO assignment(name,grade)
 VALUES
 ('Isni', 85),
 ('Laudry', 75),
 ('Bambang', 40),
 ('Anwar', 91),
 ('Alwi', 70),
 ('Fulan', 50)

 SELECT name, marks,
 CASE
	WHEN marks > 90 THEN 'A+'
	WHEN marks > 70 THEN 'A'
	WHEN marks > 60 THEN 'B'
	WHEN marks > 40 THEN 'C'
	ELSE 'Fail'
END AS GRADE
 FROM assignment;

