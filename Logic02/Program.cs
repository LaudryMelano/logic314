﻿using System;
using System.Runtime.CompilerServices;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //IfStatement();
            //ElseStatement();
            //IfNested();
            //Ternary();
            //Switch();
            //Soal1();
            //Soal2();


            
        }

        static void Soal1()
        {
            Console.WriteLine("==== Menghitung Keluiling dan Luas Lingkaran");
            Console.Write("Masukkan Jari-jari = ");
            double r = double.Parse(Console.ReadLine());

            double phi = 3.14;

            double k = 2 * phi * r;
            double l = phi * r * r;

            Console.WriteLine("Keliling Lingkaran = " + k);
            Console.WriteLine("Luas Lingkaran = " + l);
        }


        static void Soal2()
        {
            Console.WriteLine("==== Menghitung Keluiling dan Luas Persegi");
            Console.Write("Masukkan Sisi = ");
            int s = int.Parse(Console.ReadLine());

            int k = 4 * s;
            int l = s * s;

            Console.WriteLine("Keliling Persegi = " + k);
            Console.WriteLine("Luas Persegi = " + l);

        }


        static void Switch()
        {
            Console.WriteLine("==== SWITCH =====");

            Console.Write("Masukkan buah kesukaan kamu : ");
            string buah = Console.ReadLine().ToLower();

            switch(buah)
            {
                case "pisang":
                    Console.WriteLine("Buah kesukaan kamu pisang");
                    break;
                case "apel":
                    Console.WriteLine("Buah kesukaan kamu apel");
                    break;
                case "semangka":
                    Console.WriteLine("Buah kesukaan kamu semangka");
                    break;  
                default:
                    Console.WriteLine("Buah kesukaan kamu belum terdaftar");
                    break;


            }

        }

        static void Ternary()
        {
            Console.WriteLine("==== TERNARY ====");

            Console.Write("Masukkan Nilai X = ");
            int x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y = ");
            int y = int.Parse(Console.ReadLine());

            string hasil = (x>y) ? "X lebih besar dari Y":(x<y) ? "Y lebih besar dari X" : "X dan Y sama besar";

            Console.WriteLine(hasil);
        }

        static void IfNested()
        {
            Console.WriteLine("==== IF NESTED ====");

            Console.Write("Masukkan nilai Anda : ");
            int nilai = int.Parse(Console.ReadLine());

            if (nilai >= 78 && nilai <= 89)
            {
                Console.WriteLine("Kamu Berhasil");
                if (nilai >= 90 && nilai <= 99)
                {
                    Console.WriteLine("Kamu luar Biasa");
                }
                else if (nilai == 100)
                {
                    Console.WriteLine("Kamu Sempurna");
                }
            }else if (nilai >= 0 && nilai <= 77)
            {
                Console.WriteLine("Kamu Gagal");
            }
            else
            {
                Console.WriteLine("Masukkan nilai Yang Benar!");
            }
        }

        static void ElseStatement()
        {
            Console.WriteLine("==== IF STATEMENT ====");

            int x, y;

            Console.Write("Masukkan Nilai X = ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y = ");
            y = int.Parse(Console.ReadLine());

            if ( x > y )
            {
                Console.WriteLine("Nilai X lebih besar dari Y");
            }else if( y > x )
            {
                Console.WriteLine("Nilai Y lebih besar dari X");
            }else 
            { 
                Console.WriteLine("Nilai X dan Y sama besar"); 
            }
        }

        static void IfStatement()
        {
            Console.WriteLine("==== IF STATEMENT ====");

            int x, y;

            Console.Write("Masukkan Nilai X = ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y = ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai X lebih besar dari Y");
            } 
            if (x < y) 
            {
                Console.WriteLine("Nilai Y lebih besar dari X");
            }
            if(x==y)
            {
                Console.WriteLine("Nilai X dan Y sama besar");
            }
        }
    }
}
