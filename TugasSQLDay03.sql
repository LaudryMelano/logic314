---- TUGAS SQL DAY 03 ----

CREATE DATABASE DB_Sales

CREATE TABLE salesperseon(
	id int primary key identity(1,1),
	name varchar(50) not null,
	bod date not null,
	salary decimal(18,2) not null
)

INSERT INTO salesperseon(name,bod,salary)
VALUES
('Abe', '1998/11/9', 140000),
('Bob', '1978/11/9', 44000),
('Chris', '1983/11/9', 40000),
('Dan', '1980/11/9', 52000),
('Ken', '1977/11/9', 115000),
('Joe', '1998/11/9', 38000)

CREATE TABLE orders (
id int primary key identity(1,1),
order_date date not null,
cust_id int null,
sales_person_id int not null,
amount decimal(18,2) not null
)

drop table orders

INSERT INTO orders(order_date,cust_id,sales_person_id,amount)
VALUES
('2020-8-2', 4, 2, 540),
('2020-1-22', 4, 5, 1800),
('2019-7-14', 9, 1, 468),
('2018-01-29', 7, 2, 2400),
('2021-02-03', 6, 4, 600),
('2020-03-02', 6, 4, 720),
('2021-05-06', 9, 4, 150)


--- JAWABAN -----

---a
SELECT  sal.name, count(ord.id)[Jumlah Order]
FROM salesperseon as sal
join orders as ord ON sal.id = ord.sales_person_id
group by sal.name
having count(ord.id) > 1

---b
SELECT  sal.name, sum(ord.amount)[Total Amount]
FROM salesperseon as sal
join orders as ord ON sal.id = ord.sales_person_id
group by sal.name
having sum(ord.amount) > 1000

---c
SELECT  sal.name, DATEDIFF(year, sal.bod, getdate())[umur], sal.salary, sum(ord.amount)[Total Amount]
FROM salesperseon as sal
join orders as ord ON sal.id = ord.sales_person_id
where YEAR(ord.order_date) >= 2020 
group by sal.name, sal.salary, sal.bod
order by umur

---d
SELECT  sal.name,  avg(ord.amount)[Rata-rata Total Amount]
FROM salesperseon as sal
join orders as ord ON sal.id = ord.sales_person_id
group by sal.name
order by [Rata-rata Total Amount] desc

---e
SELECT  sal.name, count(ord.sales_person_id)[Total Order], 
sal.salary*0.3[Bonus],  
sum(ord.amount)[Total Amount]
FROM salesperseon as sal
join orders as ord ON sal.id = ord.sales_person_id
group by sal.name, sal.salary
having count(ord.id) > 2 AND  sum(ord.amount) > 1000

---f
SELECT sal.name
FROM salesperseon sal
left join orders ord on sal.id = ord.sales_person_id
where ord.sales_person_id is null

---g
select sal.name, sal.salary*0.02[Potongan], sal.salary[Gaji], sal.salary - (sal.salary*0.02)[Gaji Bersih]
from salesperseon sal
left join orders ord on sal.id = ord.sales_person_id
where ord.sales_person_id is null
