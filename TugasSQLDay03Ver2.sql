---- Tugas SQL Day 03 Ver 2 ----

CREATE DATABASE DB_HR

CREATE TABLE tb_karyawan (
id bigint primary key identity(1,1),
nip varchar(50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar(50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date,
alamat varchar(100) not null,
pendidikan_terkahir varchar(50) not null,
tgl_masuk date,
)

INSERT INTO tb_karyawan (nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terkahir,tgl_masuk)
VALUES
('001', 'Hamidi', 'Samsudin', 'Pria', 'Islam', 'Sukabumi', '1977-04-21', 'Jl. Sudirman No.12', 'S1 Teknik Mesin', '2015-12-07'),
('002', 'Ghandi', 'Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl. Rambuatn No.22', 'SMA Negeri 02 Palu', '2014-01-12'),
('003', 'Paul', 'Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl. Veteran No.4', 'S1 Pendidikan Geografi', '2014-12-01')

CREATE TABLE tb_divisi (
id bigint primary key identity(1,1),
kd_divisi varchar(50) not null,
nama_divisi varchar(50) not null
)

INSERT INTO tb_divisi
VALUES
('GD', 'Gudang'),
('HRD', 'HRD'),
('KU', 'Keuangan'),
('UM', 'Umum')


CREATE TABLE tb_jabatan(
id bigint primary key identity(1,1),
kd_jabatan varchar(50),
nama_jabatan varchar(50),
gaji_pokok numeric,
tunjangan_jabatan numeric
)

INSERT INTO tb_jabatan
VALUES
('MGR', 'Manager', 5500000, 1500000),
('OB', 'Office Boy', 1900000, 200000),
('ST', 'Staff', 3000000, 750000),
('WMGR', 'Wakil Manager', 4000000, 1200000)


CREATE TABLE tb_pekerjaan(
id bigint primary key identity(1,1),
nip varchar(50) not null,
kode_jabatan varchar(50) not null,
kode_divisi varchar(50) not null,
tunjangan_kinerja numeric,
kota_penempatan varchar(50)
)


INSERT INTO tb_pekerjaan
VALUES
('001', 'ST', 'KU', 750000, 'Cianjur'),
('002', 'OB', 'UM', 350000, 'Sukabumi'),
('003', 'MGR', 'HRD', 1500000, 'Sukabumi')


--- JAWABAN ---

-- SOAL 1

select CONCAT(kr.nama_depan,' ', kr.nama_belakang) [Nama Lengkap], jb.nama_jabatan, 
jb.gaji_pokok+jb.tunjangan_jabatan[Gaji+Tunjangan]
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
where jb.gaji_pokok+pk.tunjangan_kinerja < 5000000
order by kr.jenis_kelamin

-- SOAL 2
select CONCAT(kr.nama_depan,' ', kr.nama_belakang) [Nama Lengkap], jb.nama_jabatan, div.nama_divisi,
jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja [Total Gaji], 
(jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*0.05 [Pajak],
(jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja) - (jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*0.05 [Gaji Bersih]
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
join tb_divisi div ON pk.kode_divisi = div.kd_divisi
where kr.jenis_kelamin = 'pria' AND kota_penempatan != 'sukabumi'

-- SOAL 3
select kr.nip, CONCAT(kr.nama_depan,' ', kr.nama_belakang) [Nama Lengkap], jb.nama_jabatan, div.nama_divisi,
((jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*7) * 0.25 [Bonus]
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
join tb_divisi div ON pk.kode_divisi = div.kd_divisi
order by kr.jenis_kelamin

-- SOAL 4
select kr.nip, CONCAT(kr.nama_depan, kr.nama_belakang) [Nama Lengkap], jb.nama_jabatan, div.nama_divisi,
jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja [TOTAL GAJI],
(jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*0.05 [INFAK]
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
join tb_divisi div ON pk.kode_divisi = div.kd_divisi
where jb.kd_jabatan = 'mgr'

-- SOAL 5
select kr.nip, CONCAT(kr.nama_depan,' ', kr.nama_belakang) [Nama Lengkap], jb.nama_jabatan, kr.pendidikan_terkahir, 2000000[Tunjang Pendidikan],
jb.gaji_pokok+jb.tunjangan_jabatan+2000000 [TOTAL GAJI]
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
join tb_divisi div ON pk.kode_divisi = div.kd_divisi
where kr.pendidikan_terkahir LIKE 'S1%'
order by kr.nip

-- SOAL 6
select kr.nip, CONCAT(kr.nama_depan,' ', kr.nama_belakang) [Nama Lengkap], jb.nama_jabatan, div.nama_divisi,
CASE
	WHEN jb.kd_jabatan = 'mgr' THEN ((jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*7) * 0.25
	WHEN jb.kd_jabatan = 'ST' THEN ((jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*5) * 0.25 
	ELSE ((jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja)*2) * 0.25 
END AS Bonus
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
join tb_divisi div ON pk.kode_divisi = div.kd_divisi
order by kr.jenis_kelamin

-- SOAL 7
ALTER TABLE tb_karyawan
ADD CONSTRAINT unique_nip UNIQUE (nip)

-- SOAL 8
CREATE INDEX index_nip
ON tb_karyawan (nip)

-- SOAL 9
select CONCAT(nama_depan, ' ', UPPER(nama_belakang))
from tb_karyawan
where nama_belakang like 'w%'


-- SOAL 10
select CONCAT(kr.nama_depan,' ', kr.nama_belakang) [Nama Lengkap], kr.tgl_masuk, jb.nama_jabatan, div.nama_divisi,
jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja [Total Gaji],
CASE
	WHEN DATEDIFF(year, kr.tgl_masuk, GETDATE()) >= 9 THEN (jb.gaji_pokok+jb.tunjangan_jabatan+pk.tunjangan_kinerja) * 0.1
END AS Bonus,
DATEDIFF(year, kr.tgl_masuk, GETDATE()) [Lama Bekerja]
from tb_karyawan kr
join tb_pekerjaan pk ON kr.nip = pk.nip
join tb_jabatan jb ON pk.kode_jabatan = jb.kd_jabatan
join tb_divisi div ON pk.kode_divisi = div.kd_divisi
where DATEDIFF(year, kr.tgl_masuk, GETDATE()) >= 9
order by [Lama Bekerja] desc