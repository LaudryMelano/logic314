------ TUGAS SQL DAY 02 ------

CREATE DATABASE DB_Entertainer

CREATE TABLE artis (
	kd_artis varchar(100) primary key,
	nm_artis varchar(100),
	jk varchar(100),
	bayaran bigint,
	award int,
	negara varchar(100)
)

CREATE TABLE film (
	kd_film varchar(10) primary key,
	nm_film varchar(55),
	genre varchar(55),
	artis varchar(55),
	produser varchar(55),
	pendapatan bigint,
	nominasi int
)

DROP TABLE film

CREATE TABLE produser (
	kd_produser varchar(50) primary key,
	nm_produser varchar(50),
	internasional varchar(50)
)

CREATE TABLE negara (
	kd_negara varchar(50) primary key,
	nm_negara varchar(50)
)

CREATE TABLE genre (
	kd_genre varchar(50) primary key,
	nm_genre varchar(50)
)

INSERT INTO artis (kd_artis, nm_artis, jk, bayaran, award, negara)
VALUES
('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JAKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')

INSERT INTO film 
VALUES
('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000,3),
('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000,2),
('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000,0),
('F004', 'AVENGER CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000,1),
('F005', 'SPIDERMAN HOME COMMING', 'G001', 'A001', 'PD01', 1300000000,0),
('F006', 'THER RAID', 'G001', 'A004', 'PD03', 800000000,5),
('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000,2),
('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000,4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000,3),
('F0010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000,1),
('F0011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000,0),
('F0012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000,2),
('F0013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000,5)

INSERT INTO produser
VALUES
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

INSERT INTO negara
VALUES
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

INSERT INTO genre
VALUES
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')


------- JAWABAN -------

select * from film

--- SOAL 1 -----
select pr.nm_produser, sum(fi.pendapatan) 
from produser as pr
join film as fi ON pr.kd_produser = fi.produser
where kd_produser = 'PD01'
group by nm_produser

--- SOAL 2 ----
select nm_film, nominasi
from film
where nominasi = 0 

--- SOAL 3 ----
select nm_film 
from film where nm_film like 'p%'

--- SOAL 4 ----
select nm_film 
from film where nm_film like '%y'

--- SOAL 5 ----
select nm_film 
from film where nm_film like '%d%'

--- SOAL 6 ----
select fi.nm_film, ar.nm_artis
from film as fi
join artis as ar ON fi.artis = ar.kd_artis 
order by nm_artis

--- SOAL 7 ---
select fi.nm_film, ar.negara
from film as fi
join artis as ar ON fi.artis = ar.kd_artis
where ar.negara = 'hk'

--- SOAL 8 ---
select fi.nm_film, ne.nm_negara
from film as fi
join artis as ar ON fi.artis = ar.kd_artis
join negara as ne ON ar.negara = ne.kd_negara
where not ne.nm_negara like '%o%'

--- SOAL 9 ----
select ar.nm_artis
from artis as ar
left join film as fi ON fi.artis = ar.kd_artis
where fi.artis IS NULL

select ar.nm_artis
from artis as ar
left join film as fi ON fi.artis = ar.kd_artis
group by ar.nm_artis
having count(fi.nm_film) < 1

--- SOAL 10 ---
select ar.nm_artis, ge.nm_genre
from artis as ar
join film as fi ON fi.artis = ar.kd_artis
join genre as ge ON ge.kd_genre = fi.genre
where ge.nm_genre = 'drama'

--- SOAL 11 ---
select distinct ar.nm_artis, ge.nm_genre
from artis as ar
join film as fi ON fi.artis = ar.kd_artis
join genre as ge ON ge.kd_genre = fi.genre
where ge.nm_genre = 'action' 
order by ar.nm_artis desc

select ar.nm_artis, ge.nm_genre
from artis ar
join film as fi ON fi.artis = ar.kd_artis
join genre as ge ON ge.kd_genre = fi.genre
where ge.nm_genre = 'action'
group by ar.nm_artis, ge.nm_genre
order by ar.nm_artis desc

--- SOAL 12 ---
select ne.kd_negara, ne.nm_negara, count(fi.kd_film) as Jumlah_Film
from negara as ne
left join artis as ar ON ar.negara = ne.kd_negara
left join film as fi ON fi.artis = ar.kd_artis 
group by ne.kd_negara, ne.nm_negara

--- SOAL 13 ----
select fi.nm_film 
from film as fi
join produser as pr ON pr.kd_produser = fi.produser
where pr.internasional = 'YA' AND pr.nm_produser like '%a'

--- SOAL 14 ---
select pr.nm_produser, count(fi.kd_film) as jumlah_Film
from film as fi
right join produser as pr ON pr.kd_produser = fi.produser
group by pr.nm_produser





