﻿using System;
using System.Globalization;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //RemoveString();
            //InsertString();

            //S0al1();
            //Soal2();
            //Soal3();
            //Soal4();
            //Soal5();
            //Soal6();
            //Soal7();
            //Soal8();
            //Soal9();
            //Soal10();
            //Soal12();
        }

        static void RemoveString()
        {
            Console.WriteLine("=== Remove String ===");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.Write("Isi paramater Remove : ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Remove String : {kalimat.Remove(param)}");
        }

        static void InsertString()
        {
            Console.WriteLine("=== Remove String ===");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            
            Console.Write("Isi parameter Insert : ");
            int param = int.Parse(Console.ReadLine());

            Console.Write("Isi input String : ");
            string input = Console.ReadLine();

            Console.WriteLine($"Hasiil Insert String : {kalimat.Insert(param, input)}");

        }


        static void ReplaceString2() 
        {
            Console.WriteLine("=== Replace String ===");

            Console.Write("masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.Write("dari kata : ");
            string katalama = Console.ReadLine();

            Console.Write("replace menjadi kata : ");
            string katabaru = Console.ReadLine();


            Console.WriteLine($"Hasiil Insert String : {kalimat.Replace(katalama, katabaru)}");
        }

    }
}
