﻿using System;

namespace Logic03_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal5();
            //Soal6();
            //Soal7();
            //Soal8();
            //soal9();
            //Soal10();
            //Soal11();
            //Soal12();

            CallMenu();
        }

        static void CallMenu()
        {
            bool loop = true;

            while (loop)
            {
                Console.Clear();
                Menu();
                Console.WriteLine();
                Console.Write("Ulangi proses ? (y/n) ");
                string input = Console.ReadLine().ToLower();

                if (input == "y")
                {
                    Console.Clear();
                    Menu();

                    Console.WriteLine();
                    Console.Write("Ulangi proses ? (y/n) ");
                    input = Console.ReadLine().ToLower();
                    if (input == "n")
                    {
                        loop = false;
                        Console.WriteLine("Terimakasih telah menggunakan Aplikasi kami");
                    }
                }
                else if (input == "n")
                {
                    loop = false;
                    Console.WriteLine("Terimakasih telah menggunakan Aplikasi kami");
                }
                else
                {
                    Console.WriteLine("Pilih Y/N!");
                }
            }

        }

        static void Menu() 
        {
            Console.WriteLine("==== TUGAS LOGIC ====");
            Console.WriteLine("1. Nilai Mahasiswa");
            Console.WriteLine("2. Poin Pulsa");
            Console.WriteLine("3. GrapeFood");
            Console.WriteLine("4. Shopai");
            Console.WriteLine("5. Generasi");
            Console.WriteLine("6. Cek Gaji Karyawan");
            Console.WriteLine("7. IMT");
            Console.WriteLine("8. Mean");
            Console.WriteLine("====================");

            Console.Write("Pilih Menu : ");
            string option = Console.ReadLine();

            Console.WriteLine();

            switch (option)
            {
                case "1":
                    Soal5();
                    break;
                case "2":
                    Soal6();
                    break;
                case "3":
                    Soal7();
                    break;
                case "4":
                   Soal8();
                    break;
                case "5":
                    Soal9();
                    break;
                case "6":
                    Soal10();
                    break;
                case "7":
                    Soal11();
                    break;
                case "8":
                   Soal12();
                    break;
                default:
                    Console.WriteLine("Pilih No sesuai daftar!");
                    break;
            }
        }

        static void Soal12()
        {
            Console.WriteLine("==== Selamat Datang Di Aplikasi Mean ====");
            Console.WriteLine();

            double mean;

            Console.Write("Masukkan Nilai MTK       : ");
            double mtk = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai FISIKA    : ");
            double fisika = double.Parse(Console.ReadLine());

            Console.Write("Masukkan Nilai KIMIA     : ");
            double kimia = double.Parse(Console.ReadLine());

            Console.WriteLine();

            mean = (mtk + fisika + kimia) / 3;

            Console.WriteLine();

         

            if ( mtk > 0 && fisika > 0 && kimia > 0)
            {
                Console.WriteLine($"Nilai Rata-rata : {Math.Round(mean, 2)}");
                if (mean >= 50 && mean <= 100)
                {
                    Console.WriteLine("Selamat");
                    Console.WriteLine("Kamu Berhasil");
                    Console.WriteLine("Kamu Hebat");
                }
                else if (mean >= 0 && mean < 50)
                {
                    Console.WriteLine("Maaf");
                    Console.WriteLine("Kamu Gagal");
                }
                else
                {
                    Console.WriteLine("Pastikan Nilai yang Anda Masukkan sudah benar!");
                }
            }else 
            {
                Console.WriteLine("Pastikan Nilai yang Anda Masukkan sudah benar!");
            }
            
        }

        static void Soal11()
        {
            Console.WriteLine("==== Selamat Datang Di Aplikasi BMI ====");
            Console.WriteLine();

            double tbM;
            double bmi;
            string result = default;

            Console.Write("Masukkan Berat Badan Anda (kg): ");
            int bb = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Tinggi Badan Anda (cm): ");
            double tbCm = double.Parse(Console.ReadLine());

            Console.WriteLine();

            tbM = tbCm / 100;

            bmi = bb / (tbM * tbM);

            if (tbCm > 0) 
            {
                if (bmi > 0 && bmi < 18.5)
                {
                    result = "Kurus";
                }
                else if (bmi >= 18.5 && bmi < 25)
                {
                    result = "Diet";
                }
                else if (bmi >= 25)
                {
                    result = "Obesitas";
                }
                else
                {
                    Console.WriteLine("Masukkan angka tinggi badan  yang sesuai!");
                }
            }
            else
            {
                Console.WriteLine("Masukkan angka berat badan yang sesuai!");
            }

            Console.WriteLine($"Anda termasuk berbadan {result}");
           
        }

        static void Soal10()
        {
            Console.WriteLine("==== Selamat Datang Di Aplikasi Gaji Karyawan ====");
            Console.WriteLine();

            double pajak=0;
            double bpjs;
            double gajiPerbulan;
            double totalGaji;
            int gaji;

            Console.Write("Masukkan Nama : ");
            string nama = Console.ReadLine();

            Console.Write("Masukkan Tunjangan : Rp.");
            int tunjangan = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Gaji Pokok : Rp.");
            int gapok = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Banyak Bulan bekerja : ");
            int bulan = int.Parse(Console.ReadLine());

            Console.WriteLine();

            gaji = gapok + tunjangan;
            bpjs = 0.03 * gaji;

            if (gaji > 0 && gaji <= 5000000)
            {
                pajak = 0.05 * gaji;
                
            }
            else if (gaji > 5000000 && gaji <= 10000000)
            {
                pajak = 0.1 * gaji;
            }
            else if (gaji > 10000000)
            {
                pajak = 0.15 * gaji;
            }
            else
            {
                Console.WriteLine("Masukkan Data yang benar!");
            }

            gajiPerbulan = gaji - (pajak+bpjs);
            totalGaji = gaji - (pajak + bpjs) * bulan;

            Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut :");
            Console.WriteLine($"Pajak                     : Rp.{pajak}");
            Console.WriteLine($"BPJS                      : Rp.{bpjs}");
            Console.WriteLine($"Gaji perBulan             : Rp.{gajiPerbulan}");
            Console.WriteLine($"Total Gaji/Banyak bulan   : Rp.{totalGaji}");
        }

        static void Soal9()
        {
            Console.WriteLine("==== Selamat Datang Di Aplikasi Cek Generasi ====");
            Console.WriteLine();
            Console.Write("Masukkan Nama Anda : ");
            string nama = Console.ReadLine();

            Console.Write("Tahun berapa Anda lahir : ");
            int tahunLahir = int.Parse(Console.ReadLine());
            Console.WriteLine();

            if (tahunLahir >= 1944 && tahunLahir <= 1964)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir, Anda tergolong Generasi Baby Boomer");
            }
            else if (tahunLahir >= 1965 && tahunLahir <= 1979)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir, Anda tergolong Generasi X");
            }
            else if (tahunLahir >= 1980 && tahunLahir <= 1994)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir, Anda tergolong Generasi Y");
            }
            else if (tahunLahir >= 1995 && tahunLahir <= 2015)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir, Anda tergolong Generasi Z");
            }
            else
            {
                Console.WriteLine($"{nama}, Maaf keterangan generasi untuk tahun lahir Anda belum tersedia");
            }
        }

        static void Soal8()
        {

            Console.WriteLine("==== Selamat Datang Di Shoppai ====");
            Console.WriteLine();
            Console.WriteLine("==== PROMO DISKON LEBARAN ====");
            Console.WriteLine("Vocher 1 min. Order 30.000");
            Console.WriteLine("Vocher 2 min. Order 50.000");
            Console.WriteLine("Vocher 3 min. Order 100.000");
            Console.WriteLine();

            Console.Write("Masukkan Nominal Belanja : Rp.");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Ongkos Kirim : Rp.");
            int ongkir = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int diskonB1 = 5000;
            int diskonB2 = 10000;
            int diskonB3 = 10000;
            int disOngkir1 = 5000;
            int disOngkir2 = 10000;
            int disOngkir3 = 20000;
            int disOngkir = 0;
            int hasilOngkir;
            int totalBelanja = 0;
            int disBelanja = 0;

            if (ongkir > 0 && belanja > 0) 
            {
                if (belanja >= 30000 && belanja < 50000)
                {
                    Console.WriteLine("Selamat Anda mendapatkan voucher diskon");
                    Console.WriteLine("Vocher 1 : Diskon Ongkir 5rb & Diskon Belanja 5rb");
                    Console.WriteLine();

                    Console.Write("Pilih Voucher : ");
                    int voucher = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    if (voucher == 1)
                    {
                        hasilOngkir = ongkir - disOngkir1;
                        if(hasilOngkir <= 0)
                        {
                            totalBelanja = belanja - diskonB1;
                            disBelanja = diskonB1;
                        }else
                        {
                            totalBelanja = (belanja - diskonB1) + (ongkir-hasilOngkir);
                            disOngkir = disOngkir1;
                            disBelanja = diskonB1;
                        }
                    }
                }
                else if (belanja >= 50000 && belanja < 100000)
                {
                    Console.WriteLine("Selamat Anda mendapatkan voucher diskon");
                    Console.WriteLine("Vocher 1 : Diskon Ongkir 5rb & Diskon Belanja 5rb");
                    Console.WriteLine("Vocher 2 : Diskon Ongkir 10rb & Diskon Belanja 10rb");
                    Console.WriteLine();

                    Console.Write("Pilih Voucher : ");
                    int voucher = int.Parse(Console.ReadLine());
                    Console.WriteLine();

                    if (voucher == 1)
                    {
                        hasilOngkir = ongkir - disOngkir1;
                        if (hasilOngkir <= 0)
                        {
                            totalBelanja = belanja - diskonB1;
                            disOngkir = disOngkir1;
                            disBelanja=diskonB1;
                        }
                        else
                        {
                            totalBelanja = (belanja - diskonB1) + (ongkir - hasilOngkir);
                            disOngkir = disOngkir1;
                            disBelanja = diskonB1;
                        }
                    }
                    else if (voucher == 2)
                    {
                        hasilOngkir = ongkir - disOngkir2;
                        if (hasilOngkir <= 0)
                        {
                            disBelanja = diskonB2;
                            totalBelanja = belanja - diskonB2;
                            disOngkir = disOngkir2;
                        }
                        else
                        {
                            disBelanja = diskonB2;
                            totalBelanja = (belanja - diskonB2) + (ongkir - hasilOngkir);
                            disOngkir = disOngkir2;
                        }
                    }
                }
                else if (belanja >= 100000)
                {
                    Console.WriteLine("Selamat Anda mendapatkan voucher diskon");
                    Console.WriteLine("Vocher 1 : Diskon Ongkir 5rb & Diskon Belanja 5rb");
                    Console.WriteLine("Vocher 2 : Diskon Ongkir 10rb & Diskon Belanja 10rb");
                    Console.WriteLine("Vocher 3 : Diskon Ongkir 20rb & Diskon Belanja 10rb");
                    Console.WriteLine();

                    Console.Write("Pilih Voucher : ");
                    int voucher = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    if (voucher == 1)
                    {
                        hasilOngkir = ongkir - disOngkir1;
                        if (hasilOngkir <= 0)
                        {
                            totalBelanja = belanja - diskonB1;
                            disOngkir = disOngkir1;
                            disBelanja = diskonB1;
                        }
                        else
                        {
                            totalBelanja = (belanja - diskonB1) + (ongkir - hasilOngkir);
                            disOngkir = disOngkir1;
                            disBelanja = diskonB1;
                        }
                    }
                    else if (voucher == 2)
                    {
                        hasilOngkir = ongkir - disOngkir2;
                        if (hasilOngkir <= 0)
                        {
                            totalBelanja = belanja - diskonB2;
                            disOngkir = disOngkir2;
                            disBelanja = diskonB2;
                        }
                        else
                        {
                            totalBelanja = (belanja - diskonB2) + (ongkir - hasilOngkir);
                            disOngkir = disOngkir2;
                            disBelanja = diskonB2;
                        }
                    }
                    else if (voucher == 3)
                    {
                        hasilOngkir = ongkir - disOngkir3;
                        if (hasilOngkir <= 0)
                        {
                            totalBelanja = belanja - diskonB3;
                            disOngkir = disOngkir3;
                            disBelanja = diskonB3;
                        }
                        else
                        {
                            totalBelanja = (belanja - diskonB3) + (ongkir - hasilOngkir);
                            disOngkir = disOngkir3;
                            disBelanja = diskonB3;
                        }
                    }
                }
                else if (belanja>0 && belanja<30000)
                {
                    totalBelanja = belanja + ongkir;
                }
                else
                {
                    Console.WriteLine("Periksa kembali Nominal belanja Anda!");
                }

                Console.WriteLine("======= INVOICE =======");
                Console.WriteLine($"Belanja         : Rp.{belanja}");
                Console.WriteLine($"Ongkos Kirim    : Rp.{ongkir}");
                Console.WriteLine($"Diskon Ongkir   : Rp.{disOngkir}");
                Console.WriteLine($"Diskon Belanja  : Rp.{disBelanja}");
                Console.WriteLine($"Total Belanja   : Rp.{totalBelanja}");
            }
            else
            {
                Console.WriteLine("Masukkan nominal ongkir atau belanja yang benar!");
            }

            
        }

        static void Soal7()
        {
            double diskon = 0.4;
            int ongkir = 5000;
            int ongkirT = 1000;
            int maxDiskon = 30000;
            double hasilDiskon = 0;
            int jarakT = 0;

            Console.WriteLine("==== SELAMAT DATANG DI GRAPE ====");
            Console.WriteLine();

            Console.Write("Masukan Nominal Belanja : Rp.");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Jarak : ");
            int jarak = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Promo : ");
            string promo = Console.ReadLine().ToUpper();
            Console.WriteLine();

            hasilDiskon = belanja * diskon;

            if (promo == "JKTOVO" && belanja >= 30000)
            {
                if (jarak >= 1 && jarak <= 5)
                {
                    if (hasilDiskon > 0 && hasilDiskon <= 30000)
                    {
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja                 : Rp.{belanja}");
                        Console.WriteLine($"Diskon 40% (Max.30rb)   : Rp.{hasilDiskon}");
                        Console.WriteLine($"Ongkir                  : Rp.{ongkir}");
                        Console.WriteLine($"Total Belanja           : Rp.{belanja - hasilDiskon + ongkir}");
                    }
                    else
                    {
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja                 : Rp.{belanja}");
                        Console.WriteLine($"Diskon 40% (Max.30rb)   : Rp.{maxDiskon}");
                        Console.WriteLine($"Ongkir                  : Rp.{ongkir}");
                        Console.WriteLine($"Total Belanja           : Rp.{belanja - maxDiskon + ongkir}");
                    }
                }
                else if(jarak>5)
                {
                    if (hasilDiskon > 0 && hasilDiskon <= 30000)
                    {
                        jarakT = jarak - 5;
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja                 : Rp.{belanja}");
                        Console.WriteLine($"Diskon 40% (Max.30rb)   : Rp.{hasilDiskon}");
                        Console.WriteLine($"Ongkir                  : Rp.{ongkir + (ongkirT * jarakT)}");
                        Console.WriteLine($"Total Belanja           : Rp.{belanja - hasilDiskon + ongkir + (ongkirT * jarakT)}");
                    }
                    else
                    {
                        jarakT = jarak - 5;
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja                 : Rp.{belanja}");
                        Console.WriteLine($"Diskon 40% (Max.30rb)   : Rp.{maxDiskon}");
                        Console.WriteLine($"Ongkir                  : Rp.{ongkir + (ongkirT * jarakT)}");
                        Console.WriteLine($"Total Belanja           : Rp.{belanja - maxDiskon + ongkir + (ongkirT * jarakT)}");
                    }
                }
                else
                {
                    Console.WriteLine("Periksa kembali jarak yang Anda masukkan!");
                }
            }
            else if (belanja >= 30000)
            {
                if (promo == " " || promo != "JKTOVO")
                {
                    if (jarak >= 1 && jarak <= 5)
                    {
                        Console.WriteLine("Maaf Promo tidak valid");
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja         : Rp.{belanja}");
                        Console.WriteLine($"Ongkir          : Rp.{ongkir}");
                        Console.WriteLine($"Total Belanja   : Rp.{belanja + ongkir}");
                    }
                    else if (jarak > 5)
                    {
                        jarakT = jarak - 5;
                        Console.WriteLine("Maaf Promo tidak valid");
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja         : Rp.{belanja}");
                        Console.WriteLine($"Ongkir          : Rp.{ongkir + (ongkirT * jarakT)}");
                        Console.WriteLine($"Total Belanja   : Rp.{belanja + ongkir + (ongkirT * jarakT)}");
                    }
                    else
                    {
                        Console.WriteLine("Periksa kembali jarak yang Anda masukkan!");
                    }
                }
                else
                {
                    Console.WriteLine("Periksa kembali promo yang Anda masukkan!");
                }
            }else if(belanja > 0 && belanja < 30000)
            {
                if (promo == " " || promo != "JKTOVO" || promo == "JKTOVO")
                {
                    if (jarak >= 1 && jarak <= 5)
                    {
                        Console.WriteLine("Maaf Promo tidak valid");
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja         : Rp.{belanja}");
                        Console.WriteLine($"Ongkir          : Rp.{ongkir}");
                        Console.WriteLine($"Total Belanja   : Rp.{belanja + ongkir}");
                    }
                    else if (jarak > 5)
                    {
                        jarakT = jarak - 5;
                        Console.WriteLine("Maaf Promo tidak valid");
                        Console.WriteLine("======= INVOICE =======");
                        Console.WriteLine($"Belanja         : Rp.{belanja}");
                        Console.WriteLine($"Ongkir          : Rp.{ongkir + (ongkirT * jarakT)}");
                        Console.WriteLine($"Total Belanja   : Rp.{belanja + ongkir + (ongkirT * jarakT)}");
                    }
                    else
                    {
                        Console.WriteLine("Periksa kembali jarak yang Anda masukkan!");
                    }
                }
            } else
            {
                Console.WriteLine("Periksa kembali nominal Belanja yang Anda masukkan!");
            }
        }

        static void Soal6()
        {
            Console.WriteLine("==== SELAMAT DATANG DI BLESSING CELL ====");
            Console.WriteLine();
            Console.Write("Masukkan Nominal Pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());
            Console.WriteLine();

            if (pulsa >= 5000 && pulsa < 10000)
            {
                Console.WriteLine($"Pembelian pulsan Anda : {pulsa}");
                Console.WriteLine("Maaf Anda belum mendapatkan Poin");
            }
            else if (pulsa >= 10000 && pulsa < 25000)
            {
                Console.WriteLine($"Pembelian pulsa Anda : {pulsa}");
                Console.WriteLine($"Poin yang didapatkan : {80}");
            }
            else if (pulsa >= 25000 && pulsa < 50000)
            {
                Console.WriteLine($"Pembelian pulsa Anda : {pulsa}");
                Console.WriteLine($"Poin yang didapatkan : {200}");
            }
            else if (pulsa >= 50000 && pulsa < 100000)
            {
                Console.WriteLine($"Pembelian pulsa Anda : {pulsa}");
                Console.WriteLine($"Poin yang didapatkan : {400}");
            }
            else if (pulsa >= 100000)
            {
                Console.WriteLine($"Pembelian pulsan Anda : {pulsa}");
                Console.WriteLine($"Poin yang didapatkan : {800}");
            }
            else
            {
                Console.WriteLine("Maaf Pulsa dengan Nominal tersebut tidak tersedia");
            }
        }

        static void Soal5()
        {
            Console.WriteLine("==== APLIKASI GRADE NILAI MAHASISWA ====");
            Console.WriteLine();
            Console.Write("Masukkan Nilai Mahasiswa : ");
            int nilai = int.Parse(Console.ReadLine());
           

            if (nilai >= 90 && nilai <= 100)
            {
                Console.WriteLine("A");
            }
            else if (nilai >= 70 && nilai < 90)
            {
                Console.WriteLine("B");
            }
            else if (nilai >= 50 && nilai < 70)
            {
                Console.WriteLine("C");
            }
            else if (nilai >= 0 && nilai < 50)
            {
                Console.WriteLine("E");
            }
            else
            {
                Console.WriteLine("Masukkan Nilai yang benar!");
            }
        }
    }
}
