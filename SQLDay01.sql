--CREATE DATABASE--
CREATE DATABASE db_kampus

use db_kampus


--CREATE TABLE--
CREATE TABLE mahasiswa (
id bigint primary key identity (1,1),
name varchar(50) not null,
addrress varchar(50) not null,
email varchar (255) null
)

CREATE TABLE biodata (
id bigint primary key identity (1,1),
dob datetime not null,
kota varchar(100) null
)

CREATE TABLE penjualan (
id bigint primary key identity (1,1),
nama varchar(100) not null,
harga int not null
)

--CREATE VIEW--
CREATE VIEW vMahasiswa
as SELECT id,name,addrress FROM mahasiswa

SELECT * from vMahasiswa

------------------------------------

--ALTER ADD COLUMN--
ALTER TABLE mahasiswa ADD [description] varchar(255)
ALTER TABLE biodata ADD mahasiswa_id bigint not null
ALTER TABLE mahasiswa ADD panjang smallint 

--ALTER DROUP COLUMN--
ALTER TABLE mahasiswa DROP COLUMN [description]

--TABLE ALTER COLUMN--
ALTER TABLE mahasiswa ALTER COLUMN email VARCHAR (100)
ALTER TABLE biodata ALTER COLUMN dob date not null

---------------------------------

--DROP DATABASE--
DROP DATABASE db_kampus2


--DROP TABLE--
DROP TABLE mahasiswa2

--DROP VIEW--
DROP VIEW [namaVie]

---------------------------------

--------------DML----------------


----------- INSERT ---------
INSERT INTO mahasiswa (name,addrress,email) VALUES 
('Marchell','Medan','Marchel@gmail.com'), 
('Toni','Garut','Toni@gmail.com'),
('Isni','Cimahi','Isni@gmail.com')

INSERT INTO biodata (dob,kota,mahasiswa_id) VALUES 
('2000-03-15','Jakarta Barat',1), 
('1998-08-05','Grogol',4), 
('2000-06-26','Kebayoran',3)

INSERT INTO penjualan (nama,harga) VALUES
('Indomie', 1500),
('Close Up', 3500),
('Pepsodent', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sampoerna', 11000),
('Rokok 234', 11000)

----------- SELECT ---------
SELECT id,name,addrress,email FROM mahasiswa
SELECT * FROM mahasiswa
SELECT * FROM biodata

----------- UPDATE ---------
UPDATE mahasiswa SET name = 'Ilham', addrress = 'Jakarta' WHERE id=2
UPDATE biodata SET dob = '2000-03-15' WHERE id=1
UPDATE biodata SET dob = '2000-06-26' WHERE id=3
UPDATE biodata SET dob = '1998-08-05' WHERE id=4
UPDATE mahasiswa SET panjang = 48 WHERE id=1
UPDATE mahasiswa SET panjang = 86 WHERE id=4
UPDATE mahasiswa SET panjang = 117 WHERE id=3
UPDATE mahasiswa SET panjang = 50 WHERE id=5

---------- DELETE ----------
DELETE FROM mahasiswa WHERE id=2

----------- JOIN -------------
SELECT mhs.name, MONTH(bio.dob), YEAR(bio.dob) FROM mahasiswa as mhs 
JOIN biodata as bio 
ON mhs.id = bio.mahasiswa_id WHERE mhs.name = 'isni'

----------- AND -------------
SELECT mhs.name, MONTH(bio.dob) as BulanLahir, YEAR(bio.dob) as TahunLahir FROM mahasiswa as mhs 
JOIN biodata as bio 
ON mhs.id = bio.mahasiswa_id WHERE mhs.name = 'isni' AND bio.kota = 'kebayoran'

----------- ORDER BY ----------
SELECT* FROM mahasiswa as mhs 
JOIN biodata as bio 
ON mhs.id = bio.mahasiswa_id ORDER BY mhs.id ASC, mhs.name DESC

----------- SELECT TOP  ----------
SELECT TOP 1 * FROM mahasiswa ORDER BY name DESC


----------- BETWEEN ---------
SELECT * FROM mahasiswa WHERE id BETWEEN 1 AND 3
SELECT * FROM mahasiswa WHERE id >= 1 AND id <= 3

--------- LIKE ------------
SELECT * FROM mahasiswa WHERE name like 'm%'
SELECT * FROM mahasiswa WHERE name like '%i'
SELECT * FROM mahasiswa WHERE name like '%sn%'
SELECT * FROM mahasiswa WHERE name like '_o%'
SELECT * FROM mahasiswa WHERE name like '__n%'
SELECT * FROM mahasiswa WHERE name like 't_%'
SELECT * FROM mahasiswa WHERE name like 't__%'
SELECT * FROM mahasiswa WHERE name like 'i%i'

--------- GROUP BY ------------
SELECT name,addrress,sum(id) as newID FROM mahasiswa GROUP BY name,addrress

----------- HAVING ------------
SELECT name, count(id) FROM mahasiswa 
GROUP BY name
HAVING count(id)>1

----------- DISTINCT -----------
SELECT DISTINCT name FROM mahasiswa

----------- SUBSTRING ----------
SELECT SUBSTRING('SQL TUTORIAL', 1,3) AS judul

----------- CHAR INDEX ---------
SELECT CHARINDEX('t', 'Customer') as judul

----------- DATA LENGTH --------
SELECT DATALENGTH('aku mau istirahat')

----------- WHEN CASE ----------
SELECT id,name,addrress,panjang,
CASE 
	WHEN panjang < 50 THEN 'PENDEK'
	WHEN panjang <= 100 THEN 'SEDANG'
	ELSE'TINGGI'
END AS TinggiBadan
FROM mahasiswa

----------- CONCAT ----------
SELECT CONCAT ('SQL', ' is', ' fun!')
SELECT 'SQL' + 'IS' + 'FUN'

----------- OPERATOR ARITMATIKA ----------
SELECT nama, harga, harga*100 as HargaPer100Pcs 
FROM penjualan


----------- PLUS PLUS ---------------
select *, ROW_NUMBER() over(order by nama) as nomor_urut 
from tblPengarang

---------- Melihat Struktur Column ------------
sp_columns 'tblPengarang'