﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            soal3_2();
        }

        static void soal1()
        {
            Console.WriteLine("\t\t======= JARAK TEMPUH OJOL =======");
            Console.WriteLine();

            Console.WriteLine("\t1. Jarak dari Toko ke Customer 1       = 2 KM");
            Console.WriteLine("\t2. Jarak dari Customer 1 ke Customer 2 = 5 M");
            Console.WriteLine("\t3. Jarak dari Customer 2 ke Customer 3 = 1,5 KM");
            Console.WriteLine("\t4. Jarak dari Customer 3 ke Customer 4 = 3 M");
            Console.WriteLine();

        loop:
            Console.Write("\tPilih jarak yang ditempuh Ojol     : ");
            int jarak = int.Parse(Console.ReadLine());

            double jarak1 = 2000, jarak2 = 500, jarak3 = 1500, jarak4 = 300;

            double totalJarak = 0, bensin = 2500;
            int totalBensin = 0;

            string tmp="";
        

            if (jarak == 1) 
            {
                
                totalJarak = jarak1;
                tmp += jarak1/1000 + "KM";
            }else if (jarak == 2)
            {
                totalJarak = jarak1+jarak2;
                tmp += jarak1/1000 + "KM" + " + " + jarak2 + "M";
            }
            else if (jarak == 3)
            {
                totalJarak = jarak1+jarak2+jarak3;
                tmp += jarak1/1000 + "KM" + " + " + jarak2+ "M" + " + " + jarak3/1000 + "KM";
            }
            else if (jarak == 4)
            {
                totalJarak = jarak1+jarak2+jarak3+jarak4;
                tmp += jarak1 / 1000 + "KM" + " + " + jarak2 + "M" + " + " + jarak3 / 1000 + "KM" + " + " + jarak4 + "M";
            }
            else
            {
                Console.WriteLine("\tInvalid!, Harap pilih jarak yang sesuai!");
                Console.WriteLine();
                goto loop;
            }

            Console.WriteLine();
            Console.WriteLine("\tJarak yang ditempuh Ojol           : " + tmp + " = " + totalJarak / 1000 + "KM");

            if (totalJarak/1000 >= bensin/1000)
            {
                totalBensin += 1;
                while (totalJarak / 1000 > bensin / 1000)
                {
                    totalJarak -= bensin;
                    totalBensin++;
                }
            }
            else
            {
                totalBensin = 1;
            }

          
            Console.WriteLine("\tBensin yang diperlukan Ojol        : " + totalBensin + "L");

        }

        static void soal2()
        {
            Console.WriteLine("\t\t======= MEMBUAT KUE PUKIS =======");
            Console.WriteLine();

            Console.Write("\tMasukkan jumlah kue pukis yang ingin dibuat : ");
            int pukis = int.Parse(Console.ReadLine());

            double terigu = 115, gula = 190, susu = 100;
            
                terigu = terigu / 15 * pukis ;
                gula = gula / 15 * pukis;
                susu = susu / 15 * pukis;

            Console.WriteLine();
            Console.WriteLine($"\tBahan yang diperlukan untuk membuat  {pukis} pukis adalah : ");
            Console.WriteLine("\tTerigu  : " + Math.Round(terigu) + " gr");
            Console.WriteLine("\tGula    : " + Math.Round(gula) + " gr");
            Console.WriteLine("\tSusu    : " + Math.Round(susu) + " ml");
        }

        static void soal3()
        {
            Console.WriteLine("\t======== PYRAMID ANGKA ==========");
            Console.WriteLine();

            Console.Write("\tMasukkan jumlah deret : ");
            int deret = int.Parse(Console.ReadLine());

            Console.Write("\tMasukkan angka deret  : ");
            int angka = int.Parse(Console.ReadLine());

            int tambah = angka;
            int tambah2 = angka;
            string tmp = default;


            for (int i = 0; i < deret; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    if (j == 0)
                    {
                        tmp += "\t" + angka + "\t";
                    }
                    else if (i == j)
                    {
                        tambah += angka;
                        tmp += tambah;
                    }else if (i == deret-1)
                    {
                        tambah2 += angka;
                        tmp += tambah2 + "\t";
                    }
                    else
                    {
                        tmp += "\t";
                    }
                }
                tmp += "\n\n";
            }

            Console.WriteLine();
            Console.WriteLine(tmp);
        }

        static void soal3_2()
        {
            Console.WriteLine("\t======== PYRAMID ANGKA ==========");
            Console.WriteLine();

            Console.Write("\tMasukkan jumlah deret : ");
            int deret = int.Parse(Console.ReadLine());

            Console.Write("\tMasukkan angka deret  : ");
            int angka = int.Parse(Console.ReadLine());

           
            string tmp = default;


            for (int i = 0; i < deret; i++)
            {
                int tambah = 0;
                for (int j = 0; j <= i; j++)
                {
                    tambah += angka;
                    if (j == 0 || i == deret - 1 || i == j)
                    {
                        tmp += tambah + "\t";
                    }
                    else
                    {
                        tmp += "\t";
                    }
                }
                tmp += "\n\n";
            }

            for (int i = 0; i < deret; i++)
            {
                int tambah = 0;
                for (int j =0; j < deret; j++ )
                {
                    tambah += angka;
                    if (j < deret - 1 - i)
                    {
                        tmp += "\t";
                    }
                    else
                    {
                        tmp += tambah + "\t";
                    }
                }
                tmp += "\n\n";
            }

            Console.WriteLine();
            Console.WriteLine(tmp);

        }
    }
}
