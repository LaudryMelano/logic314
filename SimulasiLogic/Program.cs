﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;

namespace SimulasiLogic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //cekKapital();
            //cetakInvoice();
            //penilaian();
            //keranjangBuah();
            //pangram();
            //strongPass();
            //primeChekV2();
            //bantuan();
            //fibonaciUp();
            //arrayTarget();
            //poinPulsa();
            //recursiveDigit();
            //theOneNumber();
            //SoalKisikisi2();
            iamTheOneNumber();
        }

        static void soalRekrusifAsc()
        {
            Console.WriteLine("Masukkan Input : ");
        }

        static void SoalKisiKisi()
        {
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);

            for(int i=0; i<list.Count; i++)
            {
                list.RemoveAt(i);
            }

            Console.WriteLine(string.Join(" ", list));
        }

        static void SoalKisikisi2()
        {
            Console.Write("Masukkan Angka : ");
            int angka = int.Parse(Console.ReadLine());

            bool prime = true;
            int tmp = 0;

            for (int i = 2; i < angka; i++)
            {
                if (angka % i == 0)
                {
                    tmp = i;
                    prime = false;
                    break;
                }
            }
            if (prime)
                Console.WriteLine("1");
            else
                Console.WriteLine($"{tmp}");
        }

        static void primeChekV2()
        {
            Console.Write("Masukkan Angka : ");
            int angka = int.Parse(Console.ReadLine());

            bool prime = true;

            for (int i=2; i<angka; i++)
            {
                if (angka % i == 0)
                {
                    prime = false;
                    break;
                }

            }
                if (prime)
                    Console.WriteLine("1");
                else
                    Console.WriteLine($"Bilangan {angka} bukan bilangan prima");

            //for (int i = 2; i<=angka; i++)
            //{
            //    for (int j = 2; j<i; j++)
            //    {
            //        if (i % j == 0)
            //        {
            //            prime = false;
            //            break;
            //        }
            //    }

            //    if (prime)
            //        Console.WriteLine($"Bilangan {i} merupakan bilangan prima");
            //    prime = true;
            //}
        }

        static void PrimeCheck()
        {
            Console.Write("Masukkan Angka yang ingin di cek : ");
            int n = int.Parse(Console.ReadLine());

            int counter = PrimeCheck(n);
            if (counter == 2)
            {
                Console.WriteLine("Bilangan Prima");
            }
            else
            {
                Console.WriteLine("Bukan Bilangan Prima");
            }
        }

        static int PrimeCheck(int n)
        {
            int counter = 0;

            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    counter++;
                }
            }
            return counter;
        }

        static void strongPass()
        {
            string password = "2949148";
            char[] pass = password.ToCharArray();
            int result = 0;
            int lower = 1, upper = 1, special = 1, no = 1;
            string special_characters = "!@#$%^&*()-+";

            for (int i = 0; i < pass.Length; i++)
            {
                if (pass[i] >= 'a' && pass[i] <= 'z')
                {
                    lower = 0;
                    break;
                }

            }

            for (int i = 0; i < pass.Length; i++)
            {
                if (pass[i] >= 'A' && pass[i] <= 'Z')
                {
                    upper = 0;
                    break;
                }

            }

            for (int i = 0; i < pass.Length; i++)
            {

                if (pass[i] >= '0' && pass[i] <= '9')
                {
                    no = 0;
                    break;
                }

            }

            for (int i = 0; i < pass.Length; i++)
            {

                if (special_characters.Contains(pass[i]) && special==1)
                {
                    special = 0;
                    break;
                }

            }


            //   if (pass.Length <= 3)
            //             {
            //                 result = 6 - pass.Length;
            //             }
            //             else if (count==1 || count == 2 && pass.Length >= 4 && pass.Length <=5)
            //             {
            //                 result = 4 - count;
            //             }
            //             else
            //             {
            //                 result = 5- count;
            //             }

            result = Math.Max(6 - pass.Length, lower + no + upper + special);


          
            Console.WriteLine(result);
            Console.WriteLine(lower);
            Console.WriteLine(no);
            Console.WriteLine(upper);
            Console.WriteLine(special);

        }

        static void cekKapital() 
        {
            Console.WriteLine("===== Cek Huruf Kapital =====");
            Console.WriteLine();

            Console.Write("Masukkan kalimat yang ingin di cek : ");
            char[] kata = Console.ReadLine().ToCharArray();

            int count = 0;

            for (int i = 0; i < kata.Length; i++)
            {
                if (Char.IsUpper(kata[i]))
                {
                    count++;
                }
            }

            Console.WriteLine("Jumlah huruf kapital yang terkandung sebanyak : " +  count);

        }

        static void cetakInvoice()
        {
            Console.WriteLine("===== Cetak Invoice =====");
            Console.WriteLine();

            Console.Write("Masukkan nomor mulai : ");
            int start = int.Parse(Console.ReadLine());


            Console.Write("Masukkan nomor akhir : ");
            int end = int.Parse(Console.ReadLine());

            string tmp = default;
            char no = '0';
            char[] ints = start.ToString().ToCharArray();

            for(int i = start; i<=end; i++)
            {
                tmp += "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-" + i.ToString().PadLeft(5, no) + "\n";
            }

            //for (int i = start; i <= end; i++)
            //{ 
            //    if (ints.Length == 1) 
            //    { 
            //        tmp += "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-".PadRight(5, no)+start.ToString()+"\n";
            //    }else if(ints.Length == 2)
            //    {
            //        tmp += "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-".PadRight(4, no) + start.ToString() + "\n";
            //    }else if (ints.Length == 4)
            //    {
            //        tmp += "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-".PadRight(2, no) + start.ToString() + "\n";
            //    }
            //    else if (ints.Length == 5)
            //    {
            //        tmp += "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-".PadRight(1, no) + start.ToString() + "\n";
            //    }
            //    else
            //    {
            //        tmp += "XA-" + DateTime.Now.ToString("ddMMyyyy") + "-".PadRight(ints.Length, no) + start.ToString() + "\n";
            //    }
            //    start++;
            //}

            Console.WriteLine(tmp);
        }

        static void keranjangBuah()
        {
            Console.WriteLine("===== Keranjang Buah =====");
            Console.WriteLine();

            Console.Write("Masukkan jumlah buah di keranjang 1 : ");
            int ker1 = int.Parse(Console.ReadLine());

            Console.Write("Masukkan jumlah buah di keranjang 2 : ");
            int ker2 = int.Parse(Console.ReadLine());

            Console.Write("Masukkan jumlah buah di keranjang 3 : ");
            int ker3 = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.Write("Keranjang nomor berapa yang di bawa kepasar? : ");
            int kerPsr = int.Parse(Console.ReadLine());

            int result = 0;

            if(kerPsr == 1)
            {
                result = ker2+ker3;
            }
            else if(kerPsr == 2)
            {
                result = ker1 + ker3;
            }
            else
            {
                result = ker1 + ker2;
            }

            Console.WriteLine();
            Console.WriteLine("Sisa Buah yang tersedia : " + result);
        }

        static void pangram()
        {
            Console.WriteLine("===== PANGRAM =====");
            Console.WriteLine();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";

            Console.Write("Masukkan sebuah kalimat : ");
            string kal = Console.ReadLine().ToLower();

            //int count = 0;
            bool isPangram = true;

            //foreach ( char c in alfabet )
            //{
            //    foreach(char s in kal)
            //    {
            //        if (c == s)
            //        {
            //            count++;
            //            break;
            //        }
            //    }
            //}

            foreach (char item in alfabet)
            {
                if(!kal.Contains(item))
                {
                    isPangram=false;
                    break;
                }
            }

            if (isPangram)
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan pangram");
            }

            //if (count == 26)
            //{
            //    Console.WriteLine("Kalimat ini adalah pangram");
            //}
            //else
            //{
            //    Console.WriteLine("Kalimat ini bukan pangram");
            //    Console.WriteLine(count);
            //}

        }

        static void penilaian()
        {
            Console.WriteLine("===== Penilaian Mahasiswa =====");
            Console.WriteLine();
            Console.Write("Masukkan Nilai Mahasiswa : ");
            double[] nilai = Array.ConvertAll(Console.ReadLine().Split(","), double.Parse);

            double tmp;
            for (int i = 0; i < nilai.Length; i++)
            {
                if (nilai[i] > 35)
                {
                tmp = nilai[i] / 5;
                tmp = Math.Round(tmp, MidpointRounding.ToPositiveInfinity)*5;
                    if (tmp - nilai[i] < 3)
                    {
                        nilai[i] = tmp;
                    }
                }
            }

            foreach (double i in nilai) 
            {
                Console.WriteLine("Nilai Final : "+i);
            }
        }

        static void bantuan()
        {
            bool loop = true;
            int tlaki = 0, twanita = 0, tanak = 0, tbayi = 0;
            while (loop)
            {
                ulang:
                Console.WriteLine("===== Bantuan Banjir =====");
                Console.WriteLine("1. Laki-laki Dewasa");
                Console.WriteLine("2. Wanita Dewasa");
                Console.WriteLine("3. Anak-anak");
                Console.WriteLine("4. Bayi");
                Console.WriteLine();

               
                Console.Write("Input Baju Untuk : ");
                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        Console.Write("Jumlah Laki-laki : ");
                        int laki = int.Parse(Console.ReadLine());
                        tlaki += laki;
                        break;
                    case "2":
                        Console.Write("Jumlah Wanita : ");
                        int wanita = int.Parse(Console.ReadLine());
                        twanita = wanita * 2;
                        break;
                    case "3":
                        Console.Write("Jumlah Anak-anak : ");
                        int anak = int.Parse(Console.ReadLine());
                        tanak = anak * 3;
                        break;
                    case "4":
                        Console.Write("Jumlah Bayi : ");
                        int bayi = int.Parse(Console.ReadLine());
                        tbayi = bayi * 5;
                        break;
                    default:
                        Console.WriteLine("Pilih No sesuai daftar!");
                        break;
                }

                Console.Write("Ulangi proses ? (y/n) ");
                string input = Console.ReadLine().ToLower();

                if (input == "y")
                {
                    Console.Clear();
                    goto ulang;
                }
                else if (input == "n")
                {
                    loop = false;
                    Console.WriteLine("Terimakasih telah menggunakan Aplikasi kami");
                    int total = tlaki + twanita + tanak + tbayi;

                    if (total % 2 != 0 && total > 10)
                    {
                        total = total + (twanita/2);
                    }

                    Console.WriteLine($"Total baju yang didapatkan adalah : {total}");
                }
                else
                {
                    Console.WriteLine("Pilih Y/N!");
                }

            }
           

            
        }

        static void fibonaciUp()
        {
            Console.Write("Masukkan Maksimal himpunan : ");
            int num = int.Parse(Console.ReadLine());

            int[] numArr = new int[num];
            int[] numEven = new int[num];
            int[] numOdd = new int[num];

            int n1 = 2, n2 = 1; 
            double res1=0, res2=0, res3=0;

            for (int i = 0; i < num; i++)
            {
                if (i <= 1)
                {
                    numArr[i] = 1;
                }
                else
                {
                    numArr[i] = numArr[i - 2] + numArr[i - 1];
                }

                numEven[i] = n1;
                n1 += 2;

                numOdd[i] = n2;    
                n2 += 2;
            }
            
            for (int i = 0; i < num; i++)
            {
                res1 += numArr[i];
                res2 += numEven[i];
                res3 += numOdd[i];
            }

         
            Console.WriteLine("Fibonaci : " +string.Join(" ", numArr) + $"\tTotal : {res1}" + $"\tRata-rata : {Math.Round(res1/numArr.Length, 2)}");
            Console.WriteLine("Genap    : "+string.Join(" ", numEven) + $"\tTotal : {res2}" + $"\tRata-rata : {Math.Round(res2 / numEven.Length, 2)}");
            Console.WriteLine("Ganjil   : "+string.Join(" ", numOdd) + $"\tTotal : {res3}" + $"\tRata-rata : {Math.Round(res3 / numOdd.Length, 2)}");
        }

        static void arrayTarget()
        {
            Console.Write("Masukkan Nilai Taget : ");
            int target = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai Array : ");
            int[] numArr = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int count = 0;
            //string tmp = default;

            for (int i = 0;i < numArr.Length; i++)
            {
                for(int j = 0; j < numArr.Length; j++) 
                {
                    if (numArr[i] - numArr[j] == target)
                    {
                        count++;
                        //tmp += numArr[i];
                        //tmp += numArr[j];
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine($"Terdapat {count} pasang dalam array dengan selisih {target}");
        }

        static void recursiveDigit()
        {
            Console.Write("Masukkan deret angka : ");
            int[] deret = Array.ConvertAll(Console.ReadLine().Split(""), int.Parse);

            Console.Write("Masukkan angka perulangan : ");
            int ulang = int.Parse(Console.ReadLine());
            int totalP = 0;

                for (int i = 0; i < deret.Length; i++)
                {
                    totalP += deret[i];
                }

            totalP = totalP * ulang;

            char[] cek = new char[deret.Length];

            loop:
            if (totalP > 9)
            {
                cek = totalP.ToString().ToCharArray();
                deret = cek.Select(c => Convert.ToInt32(c.ToString())).ToArray();
                //deret = Array.ConvertAll(totalP.ToString().Split(""), int.Parse);
                totalP = 0;
                for (int i = 0; i < deret.Length; i++)
                {
                    totalP += deret[i];
                }
                goto loop;
            }

            Console.WriteLine(totalP);
        }

        static void poinPulsa()
        {
            Console.Write("Masukkan Nominal Pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());

            Console.WriteLine();

            string tmp = "";
            int poin1=0, poin2=0, poin3=0, totalPoin=0, tPulsa=0;

            if (pulsa >= 0 && pulsa <= 10000)
            {
                    tmp += poin1 + "+";
            }else if (pulsa > 10000 &&  pulsa <= 30000)
            {      
                    tPulsa = pulsa - 10000;
                    poin2 = tPulsa / 1000;
                    tmp += poin1 + "+" + poin2;
                    totalPoin = poin1 + poin2;
                    
            }else
            {
                poin2 = (30000 - 10000) / 1000;
                tPulsa = pulsa - 30000;
                poin3 = tPulsa / 1000 * 2;
                tmp += poin1 + "+" + poin2 + "+" + poin3;
                totalPoin = poin1 + poin2 + poin3;
            }

            Console.WriteLine($"Total poin yang didapatkan {tmp} = {totalPoin} Poin");
        }

        static void iamTheOneNumber()
        {
            Console.WriteLine("===== THE ONE NUMBER ====");
            Console.Write("Masukkan jumlah output : ");
            int output = int.Parse(Console.ReadLine());

            int start = 100;
            int count = 0;

            while (count<output && start <= 1000)
            {
                char[] num = start.ToString().ToCharArray();
                loop:
                int[] covNum = num.Select(x => Convert.ToInt32(x.ToString())).ToArray();
                double hitung = 0;
                for(int i = 0; i <  covNum.Length; i++)
                {
                    hitung += Math.Pow(covNum[i], 2);
                }

                if (hitung > 9)
                {
                    num = hitung.ToString().ToCharArray();
                    goto loop;
                }
                else
                {
                    if (hitung == 1)
                    {
                        Console.WriteLine(start + " is the one number");
                        count++;
                    }
                }
                start++;
            }
        }


    }
}
