﻿using System;
using System.Collections.Generic;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SimpleArraySum();
        }

        static void SimpleArraySum()
        {
            Console.WriteLine("========== SIMPLE ARRAY SUM =========");

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            //List<int> list = new List<int>() {1,2,3,4,10,11};

            int hasil = simpleArraySum(list);

            Console.WriteLine(hasil);
        }

        static int simpleArraySum(List<int> ar)
        {
            int res = 0;

            for (int i = 0; i < ar.Count; i++)
            {
                res += ar[i];
            }

            return res;
        }
    }
}
