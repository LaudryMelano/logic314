﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Contain();
            padLeft();
        }

        static void ConvertArray()
        {
            Console.WriteLine("---- Convert Array ----");
            Console.Write("Masukkan angka aaray { , }");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);

            Console.WriteLine(String.Join(",", array));
        } 

        static void padLeft()
        {
            Console.WriteLine("========= PADLEFT ===========");
            Console.Write("Masukkan Input : ");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Panjang Karakter : ");
            int panjang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Char : ");
            char chars = char.Parse(Console.ReadLine());


            Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(panjang, chars)}");

        }

        static void Contain()
        {
            Console.WriteLine("========= CONTAIN ===========");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.Write("Masukkan contain : ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung {contain}");
            }

        }
    }
}
